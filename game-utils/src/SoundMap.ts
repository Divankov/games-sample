import { AssetMap } from "./AssetMap";
import { Sound } from './Sound';

export class SoundMap extends AssetMap<Sound>
{
    protected async loadItem(url:string)
    {
        const s = new Sound();
        await s.load(url);
        return s;
    }

    protected unloadItem(item:Sound)
    {
        item.load(null);
    }

    public play(url: string | null | undefined)
    {
        if (!url) return;
        const s = this.get(url);
        s?.play();
    }
}
export interface EndConfig {
    id: number;
    image: string;
    sound?: string;
    index?: number;
}

export interface BaseGameConfig {
    endings: EndConfig[];
    backgroundTileType: 'color' | 'image';
    /** game background color */
    backgroundTileColor: string;
    /** Game background image (CSS repeat) */
    backgroundTileImage: string | null;
}

export enum StandardEndings {
    NeutralEnd = -1,
    Lose = 0,
    Win = 1
}

type PropertyType = 'enum' | 'number' | 'boolean' | 'image' | 'sound' | 'color' | 'text' | 'array' | 'font' | 'rect';

export interface PropertyDefinition<T extends BaseGameConfig>
{
    /** internal property name */
    name: keyof T;
    /** Clean, user readable title */
    title: string;
    /** Property type */
    type: PropertyType;
    /** If the property must be set */
    required?: boolean;
    /** Default value for the property */
    default?: string|number|boolean;
    /** Allowable values for an enum type */
    enumValues?: {
        value: string;
        title: string;
    }[];
    /** Allowable range for a number, if it should be limited */
    numberRange?: {
        min: number;
        max: number;
    };
    /** List of values for an array type */
    values?: PropertyDefinition<any>[]
}

export type PropertyList<T extends BaseGameConfig> = PropertyDefinition<T>[];

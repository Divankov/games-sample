import PSound from 'pixi-sound';
import { loadSound } from './utils';

export class Sound
{
    private static _isMuted:boolean = false;
    private static _devMode:boolean = false;

    public static get devMode() { return Sound._devMode; }
    public static set devMode(mode:boolean)
    {
        Sound._devMode = mode;
        if (Sound._devMode)
        {
            PSound.muteAll();
        }
        else if (!Sound._isMuted)
        {
            PSound.unmuteAll();
        }
    }

    public static get isMuted() { return Sound._isMuted; }
    public static set isMuted(mute: boolean)
    {
        Sound._isMuted = mute;
        if (Sound._isMuted)
        {
            PSound.muteAll();
        }
        else if (!Sound._devMode)
        {
            PSound.unmuteAll();
        }
    }

    public static toggleSound()
    {
        Sound.isMuted = !Sound._isMuted;
    }

    private sound: PSound.Sound|null = null;
    private url: string|null = null;

    public async load(url:string|null|undefined)
    {
        if (url === this.url) return;
        this.sound?.destroy();
        this.sound = null;
        this.url = url||null;
        if (url)
        {
            try
            {
                this.sound = await loadSound(url);
            }
            catch (e)
            {
                console.error('unable to load sound ' + url);
            }
        }
    }

    public play(): Promise<any>
    {
        return this.sound ? new Promise(resolve => this.sound!.play(resolve)) : Promise.resolve();
    }

    public loop()
    {
        if (this.sound?.isPlaying) return;
        this.sound?.play({loop: true});
    }

    public stop()
    {
        this.sound?.stop();
    }
}
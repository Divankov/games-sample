export abstract class AssetMap<T>
{
    protected byUrl: Map<string, T> = new Map();

    /** Returns list of added urls */
    public load(urls: (string|undefined|null)[]): Promise<string[]>
    {
        const promises: Promise<any>[] = [];
        const added:string[] = [];
        for (const url of urls)
        {
            if (!url) continue;

            if (!this.byUrl.has(url))
            {
                added.push(url);
                const prom = this.loadItem(url).then(t => this.byUrl.set(url, t));
                promises.push(prom);
            }
        }
        for (const url in this.byUrl)
        {
            if (!urls.includes(url))
            {
                this.unloadItem(this.byUrl.get(url)!);
                this.byUrl.delete(url);
            }
        }
        return Promise.all(promises).then(() => added);
    }

    protected abstract loadItem(url:string):Promise<T>;

    protected unloadItem(item:T)
    {
        // Should be overridden if unloading requires anything special!
    }

    public get(url: undefined|null): null;
    public get(url: string): T;
    public get(url: string|undefined|null): T|null
    {
        if (!url) return null;
        return this.byUrl.get(url)!;
    }
}
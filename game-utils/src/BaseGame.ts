import { BaseGameConfig, EndConfig } from './Interfaces'
import { getChanges, randElement } from './utils';

export interface GameOptions
{
    /** Dom element to which all game UI/canvas elements should be added to. */
    parent?: HTMLDivElement;
    /** ID of Dom element to which all game UI/canvas elements should be added to. */
    parentSelector?: string;
    /** Canvas that should be used for rendering. */
    canvas?: HTMLCanvasElement;
    /** ID of Canvas that should be used for rendering. */
    canvasSelector?: string;
}

export interface StageParams<T extends BaseGameConfig>
{
    stage: number;
    config: Partial<T>;
}

export abstract class BaseGame<T extends BaseGameConfig>
{
    public stages?: {name:string, value:number}[];

    private endings: EndConfig[] = [];

    /**
     * Callback for when the game is failed.
     */
    protected endCallback: (end: number, index?: number)=>void = ()=>{};

    /**
     * Canvas for the game to render to.
     * When constructed, this won't be assigned at the same time as parent.
     */
    protected canvas: HTMLCanvasElement;
    /**
     * Div element for the game to add children to.
     * When constructed, this won't be assigned at the same time as canvas.
     */
    protected parent: HTMLDivElement;

    protected currentProps: T;

    private _resizeId: number|null;

    constructor(options: GameOptions)
    {
        if (options.canvas)
        {
            this.canvas = options.canvas;
        }
        else if (options.canvasSelector)
        {
            this.canvas = document.querySelector(options.canvasSelector) as HTMLCanvasElement;
        }
        else if (options.parent)
        {
            this.parent = options.parent;
        }
        else if (options.parentSelector)
        {
            this.parent = document.querySelector(options.parentSelector) as HTMLDivElement;
        }
        if (!this.canvas && !this.parent)
        {
            throw new Error('BaseGame was not given a canvas or a parent!');
        }
        window.addEventListener('resize', () => {
            if (!this._resizeId)
            {
                this._resizeId = requestAnimationFrame(this._doResize);
            }
        });
        this._resizeId = null;
        this._doResize = this._doResize.bind(this);
    }

    private _doResize()
    {
        this._resizeId = null;
        if (this.parent)
        {
            this.resize(this.parent.clientWidth, this.parent.clientHeight)
        }
        else if (this.canvas)
        {
            this.resize(this.canvas.width, this.canvas.height);
        }
    }

    protected setBackground(props: {backgroundTileType:'image'|'color', backgroundTileImage: string|null, backgroundTileColor: string|null})
    {
        const target = this.parent || this.canvas;
        if (props.backgroundTileType === 'image' && props.backgroundTileImage)
        {
            target.style.backgroundImage = `url(${props.backgroundTileImage})`;
            target.style.backgroundRepeat = 'repeat';
        }
        else
        {
            target.style.backgroundImage = 'none';
        }
        if (props.backgroundTileType === 'color' && props.backgroundTileColor)
        {
            target.style.backgroundColor = props.backgroundTileColor;
        }
        else
        {
            target.style.backgroundColor = 'inherit';
        }
    }

    /**
     * Updates currentProps, updates background CSS, and returns just the changed values in the config
     */
    protected getChangesAndUpdateBackground(config: Partial<T>): Partial<T>
    {
        // if there were no previous props set, set directly
        if (!this.currentProps)
        {
            this.currentProps = config as T;
        }
        else
        {
            // otherwise discard things that are the same
            config = getChanges(config, this.currentProps);
            // and then update current props with new values
            Object.assign(this.currentProps, config);
        }
        // if the changes include any background changes, apply those
        if (config.hasOwnProperty('backgroundTileType') || config.hasOwnProperty('backgroundTileImage') ||
            config.hasOwnProperty('backgroundTileColor'))
        {
            this.setBackground(this.currentProps);
        }
        // now return the changes
        return config;
    }

    /**
     * Gets the config of a specific ending, by ID.
     * If there are multiple endings with that id, it picks a random one if index is -1, or matches index.
     */
    protected getEndConfig(id:number, index = -1): EndConfig|null
    {
        const endings = this.endings.filter(end => end.id === id);
        if (!endings.length)
        {
            return null;
        }
        if (endings.length === 1)
        {
            return endings[0];
        }
        if (index >= 0)
        {
            return Object.assign({}, endings[index], { index });
        }
        return randElement(endings);
    }

    protected setEndings(endings: EndConfig[]): void {
        this.endings = endings;
    }


    // STANDARD GAME METHODS

    /**
     * Gives the game initial config values, and any updates (with just changed properties).
     * Should resolve when the game has finished loading assets.
     */
    abstract config(config: Partial<T>): Promise<void>;

    /**
     * Called by the Widget to hand off a game end callback.
     */
    public onEndClick(callback: (end: number, index?: number) => void): void
    {
        this.endCallback = callback;
    }

    /**
     * Handles when the game display area is resized. Used as an event listener by BaseGame.
     */
    abstract resize(width: number, height: number): void;

    // LIVE PREVIEW ONLY METHODS

    /**
     * Reset game to its initial state. Does not require reloading assets. Called by the widget.
     */
    abstract restart(): void;

    /**
     * Like config, but for showing a specific stage of gameplay in stage preview mode.
     */
    abstract stage(options: StageParams<T>): Promise<void>;

    /**
     * Shows a specific ending regardless of game state. Returned promise resolves when the ending is ready
     * to be shown (fully loaded).
     */
    abstract showEnding(ending: EndConfig): Promise<void>;

    /**
     * Hides ending that was shown with showEnding().
     */
    abstract hideEnding(): void;
}

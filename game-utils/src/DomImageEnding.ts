import { EndConfig } from './Interfaces';
import PSound from 'pixi-sound';
import { loadSound, wait } from './utils';
import { Sound } from './Sound';
import { ImageMap } from './ImageMap';
import { SoundMap } from './SoundMap';

const STYLE = `
.end-image {
    display: block;
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 1;

    top: 0px;
    bottom: 0px;
    left: 0px;
    right: 0px;
}

.end-image img {
    width: 100%;
    height: 100%;
    object-fit: contain;
}

.end-image.hide {
    display: none;
}

.end-image:after {
    content: '';
    position: absolute;
    height: 100%;
    width: 100%;
    left: 0;
    top: 0;
}

.end-image.clicked:after {
    -webkit-animation-name: flash-animation;
    -webkit-animation-duration: 0.3s;

    animation-name: flash-animation;
    animation-duration: 0.3s;
}

@-webkit-keyframes flash-animation {
    from { background: white; }
    to   { background: default; }
}

@keyframes flash-animation {
    from { background: white; }
    to   { background: default; }
}
`;

const DEFAULT_WAIT = 2000;

export class DomImageEnding
{
    private callback: (end:number)=>void;
    private end: number|null;
    public image: HTMLDivElement;
    private endImages: ImageMap;
    private endSounds: SoundMap;
    public showWait: number;
    /** Unique key to track show status, so we can cancel a show() if needed */
    private showKey: symbol | null;
    private showCB?: () => void;
    public clickSound: Sound;

    constructor(showCB?: () => void)
    {
        // apply style
        this.image = document.createElement('div');
        this.image.addEventListener('click', this.onClick.bind(this));
        this.image.className = 'end-image hide';

        const style = document.createElement('style');
        style.textContent = STYLE;
        this.image.append(style);

        this.endImages = new ImageMap();
        this.endSounds = new SoundMap();
        this.showWait = DEFAULT_WAIT;
        this.end = null;
        this.showCB = showCB;

        this.clickSound = new Sound();
        this.clickSound.load('assets/endClick.mp3');
    }

    public setBackground(props: { backgroundTileType: 'image' | 'color', backgroundTileImage: string | null, backgroundTileColor: string | null })
    {
        const target = this.image;
        if (props.backgroundTileType === 'image' && props.backgroundTileImage)
        {
            target.style.backgroundImage = `url(${props.backgroundTileImage})`;
            target.style.backgroundRepeat = 'repeat';
        }
        else
        {
            target.style.backgroundImage = 'none';
        }
        if (props.backgroundTileType === 'color' && props.backgroundTileColor)
        {
            target.style.backgroundColor = props.backgroundTileColor;
        }
        else
        {
            target.style.backgroundColor = 'inherit';
        }
    }

    public async updateEnding(endings: EndConfig[])
    {
        const [addedImages, addedSounds] = await Promise.all([
            this.endImages.load(endings.map(end => end.image)),
            this.endSounds.load(endings.filter(end => end.sound).map(end => end.sound!))
        ]);

        // if currently showing, make sure that we are up to date
        if (this.end === null) return;
        // if only one end image changed, display that
        if (addedImages.length === 1)
        {
            while (this.image.childElementCount > 1)
            {
                this.image.removeChild(this.image.lastChild!);
            }
            this.image.appendChild(this.endImages.get(addedImages[0]));
        }
        // if only one end sound, play that
        if (addedSounds.length === 1)
        {
            this.endSounds.play(addedSounds[0]);
        }
    }

    public get showing() { return this.end !== null; }

    public async show(ending: EndConfig, callback: (end:number)=>void, instant = false)
    {
        this.hide();

        // don't show at all if no image present
        if (!ending.image)
        {
            return;
        }

        const myShow = Symbol();
        this.showKey = myShow;

        this.end = ending.id;
        this.callback = callback;

        const image = this.endImages.get(ending.image);

        this.endSounds.play(ending.sound);

        this.image.appendChild(image!);

        if (!instant)
        {
            await wait(this.showWait);
        }

        if (this.showKey !== myShow)
        {
            return;
        }

        this.showCB?.();

        this.image.classList.remove('hide');
        this.image.classList.remove('clicked');
    }

    private onClick()
    {
        this.callback(this.end!);

        // remove clicked class and force reflow to restart anim
        this.image.classList.remove('clicked');
        void this.image.offsetWidth;
        // add clicked class to run anim
        this.image.classList.add('clicked');
        this.clickSound.play();
        // this.hide();
    }

    public hide()
    {
        while (this.image.childElementCount > 1)
        {
            this.image.removeChild(this.image.lastChild!);
        }
        this.image.classList.add('hide');
        this.end = null;
        this.showKey = null;
    }
}
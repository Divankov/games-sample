import { AssetMap } from "./AssetMap";

export class ImageMap extends AssetMap<HTMLImageElement>
{
    protected loadItem(url:string)
    {
        return new Promise<HTMLImageElement>(resolve =>
        {
            const img = new Image();
            img.onload = () => resolve(img);
            img.src = url;
        });
    }

    protected unloadItem(item: HTMLImageElement)
    {
        item.src = '';
    }
}
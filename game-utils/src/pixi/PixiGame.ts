import {BaseGame, GameOptions, StageParams} from "../BaseGame";
import {Wrapper} from "./Wrapper";
import {Application, utils} from "pixi.js";
import {BaseGameConfig, EndConfig} from "../Interfaces";
import {Utils} from "./Utils";
import {PixiEnding} from "./PixiEnding";
import { Sound } from "../Sound";

export enum EntityBackgroundType {
    image='image',
    color="color"
}

export interface IPixiConfig extends BaseGameConfig {
    backgroundTileType: EntityBackgroundType,
    backgroundTileColor: string;
    backgroundTileImage: string,
    startSound: string,
    soundOn: string,
    soundOff: string,
    isMuted: boolean,
    controlColor: string,
}

export let pixiApp: Application;
export let pixiGame: PixiGame<IPixiConfig>;

export class PixiGame<T extends IPixiConfig> extends BaseGame<T> {

    public gameConfig: Partial<T>;
    public gameStage: number = -1;

    public wrapper: Wrapper;
    public ending: PixiEnding;

    constructor(options: GameOptions) {
        super(options);
        this.init();
    }

    protected init() {
        pixiGame = this;
        pixiApp = new Application({
            resolution: window.devicePixelRatio,
            backgroundColor: 0xFFFFFF,
            autoDensity: true,
            view: this.canvas
        });

        this.parent.appendChild(pixiApp.view);

        pixiApp.resizeTo = this.parent;
        pixiApp.ticker.add(this.update, this);

        this.wrapper = new Wrapper();
        pixiApp.stage.addChild(this.wrapper);

        this.ending = new PixiEnding(pixiApp);
        pixiApp.renderer.addListener('resize', this.renderResize, this);
    }

    protected update(frame: number) {}

    protected async createWinScene(id: number) {
        if (this.gameConfig.endings && this.gameConfig.endings.length) {
            console.log('current game has config and its engings', this.gameConfig, this.gameConfig.endings);
            // @ts-ignore
            if(this.gameConfig['completionType'] === 'unlimited') {
                console.log('type is unlimited. Check if id -1 exists.')
                const find = this.gameConfig.endings.find(item => item.id === -1);
                if (find) {
                    console.log('found ending with id -1');
                    await this.showEnding(find, false);
                }
                console.log('did not find ending with id -1');
            } else {
                const find = this.gameConfig.endings.find(item => item.id === id) || this.gameConfig.endings[0];
                await this.showEnding(find, false);
            }
        }
    }

    protected clear() {
        Utils.stopAllSound();
        this.wrapper.clear();
        this.wrapper.alpha = 1;
        this.ending.hide();
    }

    protected getChanges(partialConfig:Partial<IPixiConfig>):Partial<IPixiConfig> {
        const changes: Partial<IPixiConfig> = {};
        for (const [name, value] of Object.entries(partialConfig)) {
            // @ts-ignore
            if (this.gameConfig[name] !== value) {
                // @ts-ignore
                changes[name] = value;
            }
        }
        return changes;
    }

     protected async updateChanges(changes: Partial<IPixiConfig>): Promise<void> {
         for (const [name, value] of Object.entries(changes)) {

             switch (name) {
                 case "endings":
                     await this.updateEnding();
                     break;
                 case "backgroundTileColor":
                 case "backgroundTileImage":
                 case "backgroundTileType":

                     this.wrapper.updateBg();
                     break;
             }
         }
     }

     protected async applyStage(): Promise<void>  {
         await this.updateEnding();

         this.updateWrapper();
     }

    // ===============================
    // API
    // ===============================

    async config(partialConfig: Partial<IPixiConfig>): Promise<void> {

        if (!~this.gameStage) {
            await this.stage( {config: partialConfig, stage: 0});
        } else {
            const changes = this.getChanges(partialConfig);
            Object.assign(this.gameConfig, changes);
            await Utils.preload(changes);
            await this.updateChanges(changes);
            pixiApp.queueResize();
        }
    }

    async stage(stageParams: StageParams<IPixiConfig>) {
        this.clear();
        this.gameStage = stageParams.stage;
        Sound.devMode = stageParams.stage !== 0;
        Object.assign(this.gameConfig, stageParams.config);
        await Utils.preload(this.gameConfig);
        await this.applyStage();
        pixiApp.queueResize();
    }

    private async updateEnding() {
        if (this.gameConfig.endings) {
            await this.ending.updateEnding(this.gameConfig.endings);
        }
    }

    private updateWrapper() {
        this.wrapper.updateBg();
    }

    public async restart(): Promise<void> {
        await this.stage({config: {}, stage: this.gameStage});
    }

    public async showEnding(ending: EndConfig, instant = true): Promise<void> {
        await this.ending.show(ending, this.endCallback, instant);
    }

    public async hideEnding(): Promise<void>  {
        await this.restart();
    }

    public renderResize(): void {
        this.wrapper.resize();
        this.ending.resize();
    }

    public resize(width: number, height: number): void {}

}


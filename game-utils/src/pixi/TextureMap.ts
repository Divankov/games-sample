import { AssetMap } from "../AssetMap";
import { Texture } from 'pixi.js';

export class TextureMap extends AssetMap<Texture>
{
    protected loadItem(url:string)
    {
        return Texture.fromURL(url);
    }

    protected unloadItem(tex: Texture)
    {
        tex.destroy(true);
    }
}
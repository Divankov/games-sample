import { Container, InteractionEvent, Point, Sprite, Texture } from 'pixi.js';

export interface IPoint
{
    x: number;
    y: number;
}

const CENTER_SIZE = 8;

export class MovablePoint extends Container
{
    private center: Sprite;

    private currentDrag: Sprite|null;
    private currentPointer: number|null;
    private pointerInParent: Point|null;

    constructor(color: number, horizontalScale:number = 1, verticalScale:number = 1)
    {
        super();

        this.currentDrag = null;
        this.currentPointer = null;
        this.pointerInParent = null;

        this.center = new Sprite(Texture.WHITE);
        this.center.anchor.set(0.5);
        this.center.cursor = 'move';
        this.center.width = CENTER_SIZE * horizontalScale;
        this.center.height = CENTER_SIZE * verticalScale;

        const children = [this.center];
        for (const child of children)
        {
            child.interactive = true;
            child.tint = color;
            child.on('pointerdown', this.onPointerDown, this);
        }

        this.addChild(...children);
    }

    public reposition(x: number, y: number)
    {
        this.x = x;
        this.y = y;
    }

    public repositionToPoint(rect: IPoint)
    {
        this.reposition(rect.x, rect.y);
    }

    private onPointerDown(ev: InteractionEvent)
    {
        if (this.currentPointer) return;

        this.currentPointer = ev.data.pointerId;
        this.currentDrag = ev.target as Sprite;
        this.pointerInParent = ev.data.getLocalPosition(this.parent);

        this.currentDrag.on('pointermove', this.onPointerMove, this);
        this.currentDrag.on('pointerup', this.onPointerUp, this);
        this.currentDrag.on('pointerupoutside', this.onPointerUp, this);
        this.currentDrag.on('pointercancel', this.onPointerUp, this);
    }

    private onPointerMove(ev: InteractionEvent)
    {
        if (ev.data.pointerId !== this.currentPointer) return;

        /** Last position */
        const {x, y} = this.pointerInParent!;
        ev.data.getLocalPosition(this.parent, this.pointerInParent!);

        this.position.x += this.pointerInParent!.x - x;
        this.position.y += this.pointerInParent!.y - y;

        this.emit('move', this.position.clone());
    }

    private onPointerUp(ev: InteractionEvent)
    {
        if (ev.data.pointerId !== this.currentPointer) return;

        this.currentDrag!.off('pointermove', this.onPointerMove, this);
        this.currentDrag!.off('pointerup', this.onPointerUp, this);
        this.currentDrag!.off('pointerupoutside', this.onPointerUp, this);
        this.currentDrag!.off('pointercancel', this.onPointerUp, this);

        this.currentDrag = null;
        this.currentPointer = null;
        this.pointerInParent = null;
    }
}
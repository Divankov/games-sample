import { Container, InteractionEvent, Point, Rectangle, Sprite, Texture } from 'pixi.js';

export interface IRect
{
    x: number;
    y: number;
    width: number;
    height: number;
}

const LINE_WIDTH = 3;
const CORNER_SIZE = 5;
const CENTER_SIZE = 16;

export class ResizableRect extends Container
{
    private leftLine: Sprite;
    private rightLine: Sprite;
    private topLine: Sprite;
    private bottomLine: Sprite;
    private topLeft: Sprite;
    private topRight: Sprite;
    private bottomLeft: Sprite;
    private bottomRight: Sprite;
    private center: Container;

    private currentSize: IRect;
    private currentDrag: Sprite|null;
    private currentPointer: number|null;
    private pointerInParent: Point|null;

    constructor(color: number, icon?: string, iconColor: number = 0xffffff)
    {
        super();

        this.currentSize = {x: 0, y: 0, width: 0, height: 0};
        this.currentDrag = null;
        this.currentPointer = null;
        this.pointerInParent = null;

        this.leftLine = new Sprite(Texture.WHITE);
        this.leftLine.cursor = 'ew-resize';
        this.leftLine.width = LINE_WIDTH;

        this.rightLine = new Sprite(Texture.WHITE);
        this.rightLine.anchor.x = 1;
        this.rightLine.cursor = 'ew-resize';
        this.rightLine.width = LINE_WIDTH;

        this.topLine = new Sprite(Texture.WHITE);
        this.topLine.cursor = 'ns-resize';
        this.topLine.height = LINE_WIDTH;

        this.bottomLine = new Sprite(Texture.WHITE);
        this.bottomLine.anchor.y = 1;
        this.bottomLine.cursor = 'ns-resize';
        this.bottomLine.height = LINE_WIDTH;

        this.topLeft = new Sprite(Texture.WHITE);
        this.topLeft.cursor = 'nwse-resize';
        this.topLeft.width = this.topLeft.height = CORNER_SIZE;

        this.bottomRight = new Sprite(Texture.WHITE);
        this.bottomRight.anchor.set(1);
        this.bottomRight.cursor = 'nwse-resize';
        this.bottomRight.width = this.bottomRight.height = CORNER_SIZE;

        this.topRight = new Sprite(Texture.WHITE);
        this.topRight.anchor.x = 1;
        this.topRight.cursor = 'nesw-resize';
        this.topRight.width = this.topRight.height = CORNER_SIZE;

        this.bottomLeft = new Sprite(Texture.WHITE);
        this.bottomLeft.anchor.y = 1;
        this.bottomLeft.cursor = 'nesw-resize';
        this.bottomLeft.width = this.bottomLeft.height = CORNER_SIZE;

        this.center = new Container();
        this.center.hitArea = new Rectangle(-CENTER_SIZE / 2, -CENTER_SIZE / 2, CENTER_SIZE, CENTER_SIZE);
        this.center.cursor = 'move';

        const centerDot = new Sprite(Texture.WHITE);
        centerDot.anchor.set(0.5);
        centerDot.width = centerDot.height = CENTER_SIZE;
        centerDot.tint = color;
        this.center.addChild(centerDot);

        if (icon)
        {
            const iconSprite = new Sprite(Texture.from(icon));
            iconSprite.anchor.set(0.5);
            iconSprite.tint = iconColor;
            iconSprite.width = iconSprite.height = CENTER_SIZE;
            this.center.addChild(iconSprite);
        }

        const children = [this.center, this.leftLine, this.rightLine, this.topLine, this.bottomLine, this.topLeft,
            this.topRight, this.bottomLeft, this.bottomRight];
        for (const child of children)
        {
            child.interactive = true;
            if (child instanceof Sprite)
            {
                child.tint = color;
            }
            child.on('pointerdown', this.onPointerDown, this);
        }

        this.addChild(...children);
    }

    public resize(x: number, y: number, width: number, height: number)
    {
        this.x = this.currentSize.x = x;
        this.y = this.currentSize.y = y;
        this.currentSize.width = width = Math.max(width, CORNER_SIZE * 2);
        this.currentSize.height = height = Math.max(height, CORNER_SIZE * 2);

        this.rightLine.height = this.leftLine.height = height;
        this.bottomLine.width = this.topLine.width = width;
        this.bottomLine.y = this.bottomRight.y = this.bottomLeft.y = height;
        this.rightLine.x = this.topRight.x = this.bottomRight.x = width;
        this.center.position.set(width / 2, height / 2);

        // scale the UI bits to be more reasonably sized for larger rectangles
        const minSize = Math.min(width, height);
        // clamp size between 1 and 3
        const scale = Math.max(Math.min(4, minSize / 70), 1);
        this.center.scale.set(scale);
        this.rightLine.width = this.leftLine.width = LINE_WIDTH * scale;
        this.bottomLine.height = this.topLine.height = LINE_WIDTH * scale;
        this.topLeft.width = this.topLeft.height = this.topRight.width = this.topRight.height = this.bottomLeft.width =
            this.bottomLeft.height = this.bottomRight.width = this.bottomRight.height = CORNER_SIZE * scale;
    }

    public resizeToRect(rect: IRect)
    {
        this.resize(rect.x, rect.y, rect.width, rect.height);
    }

    private onPointerDown(ev: InteractionEvent)
    {
        if (this.currentPointer) return;

        this.currentPointer = ev.data.pointerId;
        this.currentDrag = ev.target as Sprite;
        this.pointerInParent = ev.data.getLocalPosition(this.parent);

        this.currentDrag.on('pointermove', this.onPointerMove, this);
        this.currentDrag.on('pointerup', this.onPointerUp, this);
        this.currentDrag.on('pointerupoutside', this.onPointerUp, this);
        this.currentDrag.on('pointercancel', this.onPointerUp, this);
    }

    private onPointerMove(ev: InteractionEvent)
    {
        if (ev.data.pointerId !== this.currentPointer) return;

        /** Last position */
        const {x, y} = this.pointerInParent!;
        ev.data.getLocalPosition(this.parent, this.pointerInParent!);

        let xMult = 0;
        let yMult = 0;
        let wMult = 0;
        let hMult = 0;
        // do things depending on currentDrag
        switch(this.currentDrag)
        {
            case this.leftLine:
                xMult = 1;
                wMult = -1;
                break;
            case this.rightLine:
                wMult = 1;
                break;
            case this.topLine:
                yMult = 1;
                hMult = -1;
                break;
            case this.bottomLine:
                hMult = 1;
                break;
            case this.topLeft:
                xMult = yMult = 1;
                wMult = hMult = -1;
                break;
            case this.topRight:
                yMult = 1;
                hMult = -1;
                wMult = 1;
                break;
            case this.bottomLeft:
                xMult = 1;
                wMult = -1;
                hMult = 1;
                break;
            case this.bottomRight:
                wMult = hMult = 1;
                break;
            case this.center:
                xMult = yMult = 1;
                break;
        }
        const xDiff = (this.pointerInParent!.x - x) * xMult;
        const yDiff = (this.pointerInParent!.y - y) * yMult;
        const wDiff = (this.pointerInParent!.x - x) * wMult;
        const hDiff = (this.pointerInParent!.y - y) * hMult;

        let newX = this.currentSize.x + xDiff;
        let newY = this.currentSize.y + yDiff;
        let newW = this.currentSize.width + wDiff;
        let newH = this.currentSize.height + hDiff;
        if (newW < CORNER_SIZE * 2)
        {
            newW = CORNER_SIZE * 2;
            if (xDiff)
            {
                newX = this.currentSize.x + this.currentSize.width - newW;
            }
        }
        if (newH < CORNER_SIZE * 2)
        {
            newH = CORNER_SIZE * 2;
            if (yDiff)
            {
                newY = this.currentSize.y + this.currentSize.height - newH;
            }
        }

        this.resize(newX, newY, newW, newH);

        this.emit('resize', this.currentSize);
    }

    private onPointerUp(ev: InteractionEvent)
    {
        if (ev.data.pointerId !== this.currentPointer) return;

        this.currentDrag!.off('pointermove', this.onPointerMove, this);
        this.currentDrag!.off('pointerup', this.onPointerUp, this);
        this.currentDrag!.off('pointerupoutside', this.onPointerUp, this);
        this.currentDrag!.off('pointercancel', this.onPointerUp, this);

        this.currentDrag = null;
        this.currentPointer = null;
        this.pointerInParent = null;
    }
}
import { EndConfig } from '../Interfaces';
import { Sprite, Texture, Application, Container } from 'pixi.js';
import { loadSound, wait } from '../utils';
import { Sound } from '../Sound';
import { TextureMap } from './TextureMap';
import { SoundMap } from '../SoundMap';

const DEFAULT_WAIT = 2000;

type EndingCallback = (id: number, index?: number) => void;

export class PixiEnding
{
    private callback: EndingCallback;
    private end: EndConfig|null;
    private parent: Container;
    private flash: Sprite;
    public image: Sprite;

    private app: Application;
    private endImages: TextureMap;
    private endSounds: SoundMap;
    public showWait: number;
    /** Unique key to track show status, so we can cancel a show() if needed */
    private showKey: symbol|null;
    private showCB?: () => void;
    public clickSound: Sound;

    constructor(app: Application, showCB?: () => void)
    {
        this.endImages = new TextureMap();
        this.endSounds = new SoundMap();
        this.showWait = DEFAULT_WAIT;
        this.end = null;
        this.showKey = null;
        this.parent = new Container();
        this.parent.visible = false;
        this.image = new Sprite();
        this.image.interactive = true;
        this.image.on('pointertap', this.onClick.bind(this));
        this.parent.addChild(this.image);
        this.flash = new Sprite(Texture.WHITE);
        this.parent.addChild(this.flash);

        this.showCB = showCB;

        this.clickSound = new Sound();
        this.clickSound.load('assets/endClick.mp3');

        this.app = app;
    }

    public async updateEnding(endings: EndConfig[])
    {
        const [addedImages, addedSounds] = await Promise.all([
            this.endImages.load(endings.map(end => end.image)),
            this.endSounds.load(endings.filter(end => end.sound).map(end => end.sound!))
        ]);

        // if currently showing, make sure that we are up to date
        if (this.end === null) return;
        // if only one end image changed, display that
        if (addedImages.length === 1)
        {
            this.image.visible = false;
            this.image.texture = Texture.WHITE;
            this.image.texture = this.endImages.get(addedImages[0]);
            this.image.visible = true;
            this.resize();
        }
        // if only one end sound, play that
        if (addedSounds.length === 1)
        {
            this.endSounds.play(addedSounds[0]);
        }
    }

    public get showing() { return this.end !== null; }

    public async show(ending: EndConfig, callback: EndingCallback, instant = false)
    {
        this.hide();

        // don't show at all if no image present
        if (!ending.image)
        {
            return;
        }

        const myShow = Symbol();
        this.showKey = myShow;

        this.end = ending;
        this.callback = callback;

        const image = this.endImages.get(ending.image);

        this.endSounds.play(ending.sound);

        if (!instant)
        {
            await wait(this.showWait);
        }

        if (this.showKey !== myShow)
        {
            return;
        }

        this.showCB?.();

        this.image.texture = image!;
        this.parent.visible = true;
        this.flash.alpha = 0;

        this.app.stage.addChild(this.parent);
        this.resize();
    }

    private onClick()
    {
        if (this.end) {
            this.callback(this.end.id, this.end.index);
        }

        this.clickSound.play();

        this.flash.alpha = 1;
        this.app.ticker.add(this.animateFlash, this);

        // this.hide();
    }

    private animateFlash()
    {
        // reduce to transparent over 0.3 seconds
        this.flash.alpha -= (this.app.ticker.elapsedMS / 1000) / 0.3;
        if (this.flash.alpha <= 0)
        {
            this.flash.alpha = 0;
            this.app.ticker.remove(this.animateFlash, this);
        }
    }

    public hide()
    {
        this.image.texture = Texture.WHITE;
        this.parent.visible = false;
        this.end = null;
        this.showKey = null;
        this.app.ticker.remove(this.animateFlash, this);
    }

    public resize()
    {
        this.flash.width = this.app.screen.width;
        this.flash.height = this.app.screen.height;
        const width = this.image.texture.width;
        const height = this.image.texture.height ;
        const scale = Math.min( this.app.screen.width / width, this.app.screen.height / height);
        this.image.scale.set(scale);
        this.image.x = (this.app.screen.width - width * scale) * 0.5;
        this.image.y = (this.app.screen.height - height * scale) * 0.5;
    }
}

import { Texture } from 'pixi.js';

export class ReloadableTexture
{
    private _tex: Texture| null = null;
    private url: string | null = null;

    public async load(url: string | null)
    {
        if (url === this.url) return this.texture;

        const old = this._tex;
        this._tex = null;
        this.url = url;
        if (url)
        {
            this._tex = await Texture.fromURL(url);
        }
        old &&  old.destroy(true);
        return this._tex;
    }

    public get valid()
    {
        return !!this._tex;
    }

    public get texture()
    {
        return this._tex || Texture.WHITE;
    }
}
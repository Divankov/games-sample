import Tween = gsap.core.Tween;
import { IPixiConfig, pixiApp } from "..";
import { Sound } from "../Sound";
import sound from 'pixi-sound';

export class Utils {

    public static isMute: boolean = false;
    public static setMute(value: boolean) {

        Sound.isMuted = value
        if (value) {
            Utils.stopAllSound();
        }
        Utils.isMute = value;
    }

    static async tweenAsing(tween: Tween) {
        return new Promise((resolve) => {
            tween.eventCallback("onComplete", resolve);
        });
    }

    static async preload<T extends IPixiConfig>(config: Partial<T>) {

        await new Promise((resolve) => {
            for (const value of Object.values(config)) {
                if (typeof value === "string") {
                    if (~value.indexOf(".")) {
                        this.addToLoad(value);
                    }
                }
            }
            pixiApp.loader.load(resolve);

        });
    }

    static addToLoad(data?: string) {
        data && !pixiApp.loader.resources[data] && pixiApp.loader.add(data, data);
    }

    static stopSound(url?: string) {
        url && sound.stop(url);
    }

    static playSound(url?: string, loop: boolean = false) {
        if (Utils.isMute) {
            return;
        }
        if (url && sound.exists(url) && sound.find(url)) {
            sound.play(url, { loop })
        }
    }

    static stopAllSound() {
        sound.stopAll();
    }

    public static rad2deg(angle: number): number {
        return angle * (180 / Math.PI);
    }

    public static deg2rad(angle: number): number {
        return angle * (Math.PI / 180);
    }
}


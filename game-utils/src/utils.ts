import PSound from 'pixi-sound';

export function randInt(min: number, max: number)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randElement<T>(array: T[]): T
{
    return array[randInt(0, array.length - 1)];
}

export function file2base64(file: Blob) {
    return new Promise<string>((resolve, reject) => {
        const reader = new FileReader();
        reader.addEventListener("load", () => {
                resolve(reader.result as string);
            });
        reader.addEventListener('error', (err) => {
            reject(err);
        });
        reader.readAsDataURL(file);
    });
}

export function loadResources(resources:{id:string, url:string}[]) {
    return Promise.all(
        resources.map((resource) =>
            fetch(resource.url)
                .then((response) => {
                    return response.blob();
                })
                .then((file) => {
                    return file2base64(file);
                })
        )
    ).then((loadedResources) => {
        return resources.map((resource, index) => {
            return { ...resource, base64: loadedResources[index] };
        });
    });
}

export function loadSound(url: string)
{
    return new Promise<PSound.Sound>((resolve, reject) => {
        PSound.Sound.from({
            url,
            preload: true,
            loaded: (error, sound) => {
                if (error)
                {
                    reject(error);
                    return;
                }
                resolve(sound);
            }
        })
    });
}

export function getChanges<T>(incoming: Partial<T>, current: T): Partial<T>
{
    const out: Partial<T> = {};
    for (const prop in incoming)
    {
        if (current[prop] !== incoming[prop])
        {
            out[prop] = incoming[prop];
        }
    }
    return out;
}

export function wait(milliseconds: number): Promise<void>
{
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}
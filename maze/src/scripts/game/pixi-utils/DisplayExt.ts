import * as PIXI from "pixi.js";
export default function() {


    PIXI.DisplayObject.prototype.replaceWithTransform = function(from: PIXI.DisplayObject) {
        
        from.updateTransform();
        
        if(from.parent)
            from.parent.addChildAt(this, from.parent.getChildIndex(from));
        
        this.position.copyFrom(from.position);
        this.scale.copyFrom(from.scale);
        this.rotation = from.rotation;
        
        this.updateTransform();
    }
}
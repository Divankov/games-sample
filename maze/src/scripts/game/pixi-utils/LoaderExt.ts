import * as PIXI from "pixi.js";

export default function() {
	PIXI.Loader.prototype.filter = function(func: (v: any) => boolean) {
		if (!func) return [];

		const ress = this.resources as any;

		let ret: Array<PIXI.LoaderResource> = [];

		let keys = Object.keys(ress);

		keys.forEach((k: string) => {
			if (func(ress[k])) {
				ret.push(ress[k]);
			}
		});

		return ret;
	};
}

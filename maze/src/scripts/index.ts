import {IConfig} from "./constant/Config";
import {AppStage} from "./constant/AppStage";
import {BaseGame, EndConfig, PixiEnding, Sound, StageParams} from '@imblox/game-utils'
import {defaultConfig, Stages} from "../../../slots/src/app/Config";
import {App} from "./game";
import {Menu} from "../../../slots/src/app/Menu";
import {utils} from "pixi.js";
import * as TWEEN from "@tweenjs/tween.js";
import * as PIXI from "pixi.js";
window.PIXI = PIXI;
import "pixi-spine";
import {Inject} from "pixiv5-tiled"
Inject(PIXI);


export let mainGame: Game;

export class Game extends  BaseGame<IConfig> {
    private _stage: Stages;
    private wrapper: HTMLDivElement;
    private menu: Menu;
    private app: App;
    private ending: PixiEnding;

    constructor(options: { parent: HTMLDivElement }) {
        super(options);

        this._stage = Stages.GAMEPLAY;

        this.wrapper = document.createElement('div');
        this.wrapper.style.width = '100%';
        this.wrapper.style.height = '100%';
        this.wrapper.style.position = 'relative';
        this.parent.appendChild(this.wrapper);

        this.app = (window as any).app = new App(this.wrapper);
        utils.skipHello();
        this.menu = new Menu(defaultConfig.controlColor, Sound.toggleSound);
        this.app.stage.addChild(this.menu);


        // todo need to understand if we need the following code
        //this.ending = new PixiEnding(this.app, () => {
        //    this.menu.visible = false;
        //});

    }
    private start()
    {
        this.app.ticker.add(this.update, this);
        this.menu.visible = true;
    }

    private stop()
    {
        this.app.ticker.remove(this.update, this);
    }

    private update() {
        TWEEN.update(undefined as any);
    }
    async config(config:Partial<IConfig>) {
        const tasks: Promise<any>[] = [];
        let startGame = false;
        if (!this.currentProps)
        {
            // if initial config, use default properties
            config = this.getChangesAndUpdateBackground(Object.assign({}, defaultConfig, config));
            startGame = true;
        }
        else
        {
            // otherwise record changes
            config = this.getChangesAndUpdateBackground(config);
        }
        //
        // if (config.hasOwnProperty('isMuted'))
        // {
        //     this.menu.setMute(!!config.isMuted);
        //     Sound.isMuted = !!config.isMuted;
        // }
        //
        // let texturesToDestroy: Texture[] = [];
        // let needsResize = false;
        // if (config.hasOwnProperty('machineImage'))
        // {
        //     if (config.machineImage)
        //     {
        //         texturesToDestroy.push(this.machine.texture);
        //         tasks.push(Texture.fromURL(config.machineImage!).then(tex => {
        //             this.machine.texture = tex;
        //             // if the image was changed to a new value and none of the layout values changed, reset the layout values
        //             if (!config.hasOwnProperty('reelCenterY') && !config.hasOwnProperty('reelCentersX') &&
        //                 !config.hasOwnProperty('reelRectWidth') && !config.hasOwnProperty('reelRectHeight') &&
        //                 !config.hasOwnProperty('spinButton') && !config.hasOwnProperty('payoutButton'))
        //             {
        //                 const { width, height } = tex;
        //                 setTimeout(() => {
        //                     this.payoutPreviewCB?.({x: width * 0.2, y: height * 0.8, width: width * 0.25, height: height * 0.1});
        //                     this.spinPreviewCB?.({ x: width * 0.55, y: height * 0.8, width: width * 0.25, height: height * 0.1 });
        //                     this.reelPreviewCB?.({
        //                         reelCenterY: height * 0.4,
        //                         reelCentersX: [width * 0.20, width * 0.5, width * 0.80],
        //                         reelRectWidth: width * 0.25,
        //                         reelRectHeight: width * 0.25,
        //                     });
        //                 }, 5);
        //             }
        //         }));
        //         this.machine.visible = true;
        //         needsResize = true;
        //     }
        //     else
        //     {
        //         this.machine.visible = false;
        //     }
        // }
        // if (config.hasOwnProperty('reels'))
        // {
        //     if (config.reels)
        //     {
        //         tasks.push(this.machine.handleSymbolChange(config.reels));
        //         this.chart.symbols = config.reels;
        //     }
        // }
    }
    resize(width: number, height: number)
    {
        width = Math.floor(width);
        height = Math.floor(height);
        this.app.renderer.resize(width, height);
        this.menu.x = 10;// (width - this.menu.menuWidth) / 2;
        this.menu.y = height - this.menu.menuHeight - 10;

        //this.ending.resize();
    }

    public async stage({ config, stage }: StageParams<IConfig>)
    {
        if (this.ending && this.ending.showing)
        {
            this.hideEnding();
        }
        this._stage = stage;
        Sound.devMode = stage !== Stages.GAMEPLAY;
        await this.config(config);
        this.app.load();
        this.app.on("loaded", () => {
            this.app.startGameByName('Maze');
        });

    }
    public restart(): void
    {
        // just to be safe, clean up listeners
        this.ending.hide();
        this.stop();
        // reattach listeners
        this.start();
    }

    public async showEnding(end: EndConfig)
    {
        this.stop();
        await this.ending.show(
            end,
            this.endCallback,
            true
        );
    }

    public hideEnding()
    {
        this.ending.hide();
        this.restart();
    }

}



module.exports = {Game, AppStage};

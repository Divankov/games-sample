import {EndConfig, PixiGame, StageParams, StandardEndings, Utils} from "@imblox/game-utils";
import {defaultConfig, IConfig, IRect} from "./constant/Config";
import {ScratchScene} from "./ScratchScene";
import {AppState} from "./constant/AppState";
import {AppStage} from "./constant/AppStage";
import {constant} from "./constant/Constant";
export let state: AppState = AppState.NONE;

export class Game extends PixiGame<IConfig> {

    private scratchScene: ScratchScene;

    protected init() {
        super.init();
        this.gameConfig = defaultConfig;
        this.scratchScene = new ScratchScene(this);
        this.wrapper.addScene(this.scratchScene);
    }

    protected update() {
        if (state === AppState.SCRATCHING) {
            this.scratchScene.update();

            if (this.isWin()) {
                state = AppState.WIN;
                this.scratchScene.applyFullMask();
                Utils.playSound(this.gameConfig.clearScratchSound);

                if (this.gameStage === AppStage.GAME) {
                    this.createWinScene(StandardEndings.NeutralEnd);
                }
            }
        }
    }

    private isWin(): boolean {
        return (this.scratchScene.progress || 0) >= (this.gameConfig.revealThreshold || constant.revealThreshold);
    }

    protected clear() {
        super.clear();
        state = AppState.NONE;
        this.scratchScene.clear();
    }

    protected async updateChanges(changes: Partial<IConfig>): Promise<void> {
        await super.updateChanges(changes);

        for (const [name, value] of Object.entries(changes)) {
            switch (name) {
                case "postScratchImage":
                case "scratchImage":
                case "scratchZone":
                    this.scratchScene.create();
                    break;
                case "scratchCursor":
                    this.scratchScene.createBrush();
                    break;
                case "scratchSize" :
                    this.scratchScene.updateScratchSize();
                    break;
                case "controlColor":
                    this.scratchScene.updateSoundBtn();
                    break;
                case "isMuted":
                    this.scratchScene.updateSoundBtn();
                    break;
            }
        }

    }

    protected async applyStage() {
        await super.applyStage();
        switch (this.gameStage) {
            case AppStage.GAME:
                state = AppState.SCRATCHING;
                this.scratchScene.create();
                Utils.playSound(this.gameConfig.startSound);
                break;
            case AppStage.LIVE1:
                state = AppState.NONE;
                this.scratchScene.create();
                break;
            case AppStage.LIVE2:
                state = AppState.SCRATCHING;
                this.scratchScene.create();
                break;
        }
        this.wrapper.addScene(this.scratchScene);
    }

    public async showEnding(ending: EndConfig, instant = true): Promise<void> {
        await super.showEnding(ending, instant);
        this.scratchScene.clear();
    }

    public onScratchZoneChange(callback: (rect: IRect) => void) {
        this.scratchScene.zoneChangeCallback = callback
    }
}


module.exports = {Game, AppStage};

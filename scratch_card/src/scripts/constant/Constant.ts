export const constant = {
    defaultSize: {
        width: 600,
        height: 800,
    },

    distance: 1,
    revealThreshold: 50,

    drugCircle: {
        radius: 8,
        touchRadius: 50,
    }

};

import './index.js';

export const scratchCardDefaultConfig =  {
  
        "backgroundTileType": "color",
        "backgroundTileColor": "#ff9305",
        "revealThreshold": 50,
        "scratchDensity": 100,
        "scratchSize": 100,
        "scratchZone": {
            "top": 0.43,
            "left": 0,
            "width": 1,
            "height": 0.52
        },
        "backgroundTileImage": "assets/images/tileImage.jpg",
        "scratchImage": "assets/images/scratchImage.png",
        "postScratchImage": "assets/images/postScratchImage.png",
        "startSound": "assets/sounds/start.wav",
        "scratchCursor": "assets/images/coin.png",
        "scratchSound": "assets/sounds/scratch.mp3",
        "endings": [
            {
                "id": -1,
                sound: "",
    
                "image": "assets/images/ending.png"
            }
        ]

};
const game = new Game({parent: document.querySelector('.imx-widget-canvas')});

const endingsConfig = {
            "id": -1,
            sound: "https://imblox-staging.s3.amazonaws.com/media/uploads/widgets/S5s6FGk0S7FRbkFr1O53e7-file_example_MP3_700KB.mp3",
    
            "image": "assets/images/ending.png"
        };

let lock = false;
const actions = [];
const next = document.getElementById("next");

async function nextAction() {
    if (lock) {
        console.log("WAIT! it is loading");
        return;
    }
    const action = actions.shift();
    if (action) {
        lock = true;
        await action();
        console.log("DONE!");
        lock = false
    }
}

next.onclick = nextAction;

// INIT_APP
// =========
actions.push(async () => await game.config(scratchCardDefaultConfig));
actions.push(async () => await game.stage({stage: AppStage.GAME}));

actions.push(async () => await game.stage({stage: AppStage.LIVE1}));
actions.push(async () => await game.stage({stage: AppStage.LIVE2}));
actions.push(async () => await game.showEnding(endingsConfig));

actions.push(async () => await game.stage({stage: AppStage.GAME}));
actions.push(async () => await game.config({
    "scratchImage": "assets/images/postScratchImage.png",
}));
actions.push(async () => await game.config({
    "postScratchImage": "assets/images/scratchImage.png",
}));
actions.push(async () => await game.config({backgroundTileColor: "#ff9305",
    backgroundTileImage: "assets/images/tileImage.jpg",
    backgroundTileType: "image"
}));
actions.push(async () => await game.config({backgroundTileColor: "#ff9305",
    backgroundTileImage: "",
    backgroundTileType: "image"
}));
actions.push(async () => await game.config({backgroundTileColor: "#ff9305",
    backgroundTileImage: "assets/images/tileImage.jpg",
    backgroundTileType: "image"
}));
actions.push(async () => await game.config({backgroundTileColor: "#000000",
    backgroundTileImage: "assets/images/tileImage.jpg",
    backgroundTileType: "color"
}));
actions.push(async () => await game.showEnding(endingsConfig));

actions.push(async () => await game.config(        {"endings": [
        {
            "id": -1,
        }
    ]}));
actions.push(async () => await game.config(        {"endings": [
        {
            "id": -1,
            "image": "assets/images/tileImage.jpg"
        }
    ]}));
    
actions.push(async () => await game.stage({config: {}, stage: AppStage.LIVE1}));

actions.push(async () => await game.config(        {
    "scratchImage": "",
    "postScratchImage": ""}));
actions.push(async () => await game.config(        {
    "scratchImage": "assets/images/tileImage.jpg",
    "postScratchImage": "assets/images/postScratchImage.png"}));
actions.push(async () => await game.config(        {
    "scratchImage": "assets/images/scratchImage.png",
        "postScratchImage": "assets/images/postScratchImage.png"}));
actions.push(async () => await game.config(        {
    "scratchImage": "assets/images/scratchImage.png",
    "postScratchImage": ""}));
actions.push(async () => await game.config(        {
    "scratchImage": "assets/images/scratchImage.png",
    "postScratchImage": "assets/images/postScratchImage.png"}));



// actions.push(async () => await game.config(endingsConfig));
// actions.push(async () => {
//     await game.config({
//         backgroundTileColor: "#000000",
//         backgroundTileType: "image",
//         scratchSize: 400,
//         scratchDensity: 10,
//         scratchImage: 'https://imblox.s3.amazonaws.com/media/uploads/widgets/bfo3O2LUzUX3vGcqs0b3gs-IMblox1_1.png',
//     });
// });

// INIT_APP
// =========
actions.push(async () => await game.stage({config: {}, stage: AppStage.LIVE1}));
actions.push(async () => await game.config(        {
    "scratchImage": "",
    "postScratchImage": "assets/images/postScratchImage.png"}));
actions.push(async () => await game.config(        {
    "scratchImage": "assets/images/scratchImage.png",
    "postScratchImage": "assets/images/postScratchImage.png"}));
actions.push(async () => await game.config(        {
    "scratchImage": "assets/images/scratchImage.png",
    "postScratchImage": ""}));
actions.push(async () => await game.config(        {
    "scratchImage": "assets/images/scratchImage.png",
    "postScratchImage": "assets/images/postScratchImage.png"}));

actions.push(async () => await game.showEnding(endingsConfig));
actions.push(async () => await game.hideEnding());
actions.push(async () => await game.showEnding(endingsConfig));
actions.push(async () => await game.hideEnding());

actions.push(async () => await game.stage({config: scratchCardDefaultConfig, stage: AppStage.GAME}));


// LIVE_1
// =========

actions.push(async () => game.onScratchZoneChange((rect) => {
    defaultConfig.scratchZone = rect
}));
actions.push(async () => await game.stage({config: {}, stage: AppStage.LIVE1}));

actions.push(async () => await game.config({
    scratchSize: 100,
    scratchDensity: 30,
    postScratchImage: 'https://imblox.s3.amazonaws.com/media/uploads/widgets/G3sXWoiWbAr7JNHdVXCpEa-IMblox-2_1.png',
    scratchImage: 'https://imblox.s3.amazonaws.com/media/uploads/widgets/bfo3O2LUzUX3vGcqs0b3gs-IMblox1_1.png',
}));


// LIVE_2
// =========
actions.push(async () => await game.stage({config: {}, stage: AppStage.LIVE2}));

actions.push(async () => await game.config({
    scratchSize: 50,
    scratchDensity: 100,
    postScratchImage: 'https://imblox.s3.amazonaws.com/media/uploads/widgets/G3sXWoiWbAr7JNHdVXCpEa-IMblox-2_1.png',
    scratchImage: 'https://imblox.s3.amazonaws.com/media/uploads/widgets/bfo3O2LUzUX3vGcqs0b3gs-IMblox1_1.png',
}));


actions.push(async () => await game.restart());
actions.push(async () => await game.restart());
actions.push(async () => await game.restart());

// LIVE_WIN
// =========
actions.push(async () => await game.showEnding(endingsConfig));
actions.push(async () => await game.hideEnding());
actions.push(async () => await game.showEnding(endingsConfig));
actions.push(async () => await game.hideEnding());

actions.push(async () => await game.stage({config: scratchCardDefaultConfig, stage: AppStage.GAME}));

// GAME_MODE
// =========

const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = (env, argv) => {
    const isDev = argv.mode === 'development';

    return {

        entry: './src/scripts/index.ts',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'index.js',
            libraryTarget: 'umd',
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        mode: argv.mode,
        devtool: isDev ? 'inline-source-map' : '',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                },
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new CopyPlugin({
                patterns: [
                    {from: 'src/index.html'},
                    {from: 'src/test.js'},
                    {from: 'src/css', to: 'css'},
                    {from: 'src/assets', to: 'assets'},
                ]
            }),
        ],
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 3000,
            hot: true,
            host: '0.0.0.0'
        },

        optimization: {
            minimize: !isDev
        }
    };
};

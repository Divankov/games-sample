const path = require('path');
const EsmWebpackPlugin = require("@purtuga/esm-webpack-plugin");

module.exports = (env, argv) => {
    const isDev = argv.mode === 'development';

    return {

        entry: './src/index.ts',
        output: {
            path: path.resolve(__dirname, 'parent-container'),
            filename: 'TestWizard.js',
            // this combination of library & libraryTarget with esm-webpack-plugin lets us import the library output using ES6 imports
            // in the browser
            library: 'LIB',
            libraryTarget: 'var',
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        mode: argv.mode,
        devtool: isDev ? 'inline-source-map' : '',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                },
            ]
        },
        plugins: [
            new EsmWebpackPlugin()
        ],
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 3000,
            hot: true,
            host: '0.0.0.0'
        },

        optimization: {
            minimize: !isDev
        }
    };
};

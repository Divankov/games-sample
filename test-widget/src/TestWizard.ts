import type { BaseGame, PropertyDefinition, PropertyList } from '@imblox/game-utils';

function file2base64(file: Blob)
{
    if (!file) return Promise.resolve(null);
    return new Promise<string>((resolve, reject) =>
    {
        const reader = new FileReader();
        reader.addEventListener("load", () =>
        {
            resolve(reader.result as string);
        });
        reader.addEventListener('error', (err) =>
        {
            reject(err);
        });
        reader.readAsDataURL(file);
    });
}

const STYLE = `
.prop {
    width: 100%;
    height: fit-content;
    display: inline-flex;
    flex-wrap: wrap;
    justify-content: space-between;
    padding-bottom: 5px;
    margin-bottom: 5px;
    border-bottom: 2px solid black;
}

.prop label {
    flex-shrink: 0;
    max-width: 45%;
    text-align: left;
}

.prop input, .prop .input {
    max-width: 50%;
    flex-shrink: 1;
}

.prop .row {
    width: 100%;
    flex-shrink: 0;
    display: flex;
    justify-content: space-between;
}
`;

export interface StageOption
{
    name: string;
    value: number;
}

export class TestWizard
{
    private game: BaseGame<any>;
    private parent: HTMLDivElement;

    constructor(uiParent: HTMLDivElement, game: BaseGame<any>, properties: PropertyList<any>, defaultConfig: any)
    {
        // apply style
        const style = document.createElement('style');
        style.textContent = STYLE;
        document.head.append(style);

        this.game = game;
        this.parent = uiParent;

        for (const prop of properties)
        {
            this.createUI(prop, defaultConfig[prop.name]);
        }

        if (game.stages)
        {
            const [div, select] = this.createStages(game.stages);
            this.parent.appendChild(div);
            select.addEventListener('change', () => {
                this.game.stage({stage: parseInt(select.value), config: {}});
            });
        }

        const restart = document.createElement('button');
        this.parent.appendChild(restart);
        restart.addEventListener('click', () => {
            this.game.restart();
        });
        restart.innerText = 'Restart';
    }

    private createUI(prop: PropertyDefinition<any>, defaultValue: any)
    {
        const div = document.createElement('div');
        div.className = 'prop';
        const label = document.createElement('label');
        label.innerText = prop.title;
        div.appendChild(label);
        let input: HTMLInputElement;
        switch (prop.type)
        {
            case 'boolean':
                input = document.createElement('input');
                input.type = 'checkbox';
                input.checked = !!defaultValue;
                input.addEventListener('change', () =>
                {
                    this.game.config({ [prop.name]: input.checked });
                });
                div.appendChild(input);
                break;
            case 'color':
                input = document.createElement('input');
                input.type = 'color';
                input.value = defaultValue;
                input.addEventListener('change', () =>
                {
                    this.game.config({ [prop.name]: input.value });
                });
                div.appendChild(input);
                break;
            case 'enum':
                const select = document.createElement('select');
                select.className = 'input';
                for (let i = 0; i < prop.enumValues!.length; ++i)
                {
                    const enumValue = prop.enumValues![i];
                    const opt = document.createElement('option');
                    opt.value = enumValue.value;
                    opt.innerText = enumValue.title;
                    select.appendChild(opt);
                }
                select.value = defaultValue;
                select.addEventListener('change', () => {
                    this.game.config({ [prop.name]: select.value });
                });
                div.appendChild(select);
                break;
            case 'image':
                input = document.createElement('input');
                input.type = 'file';
                input.accept = '.png,.jpg';
                input.addEventListener('change', async () =>
                {
                    this.game.config({ [prop.name]: await file2base64(input.files![0]) });
                });
                div.appendChild(input);
                break;
            case 'number':
                input = document.createElement('input');
                input.type = 'number';
                input.value = defaultValue;
                input.min = (prop.numberRange?.min ?? -100000).toString();
                input.max = (prop.numberRange?.max ?? 100000).toString();
                input.addEventListener('change', () =>
                {
                    this.game.config({ [prop.name]: parseFloat(input.value) });
                });
                div.appendChild(input);
                break;
            case 'sound':
                input = document.createElement('input');
                input.type = 'file';
                input.accept = '.mp3';
                input.addEventListener('change', async () =>
                {
                    this.game.config({ [prop.name]: await file2base64(input.files![0]) });
                });
                div.appendChild(input);
                break;
            case 'text':
                input = document.createElement('input');
                input.type = 'text';
                input.value = defaultValue;
                input.addEventListener('change', () =>
                {
                    this.game.config({ [prop.name]: input.value });
                });
                div.appendChild(input);
                break;
            case 'font':
                input = document.createElement('input');
                input.type = 'file';
                input.accept = '.woff,.woff2,.ttf';
                input.addEventListener('change', async () =>
                {
                    this.game.config({ [prop.name]: await file2base64(input.files![0]) });
                });
                div.appendChild(input);
                break;
            case 'rect':
                const props = ['x', 'y', 'width', 'height'];
                let row: HTMLDivElement;
                for (const p of props)
                {
                    [row, input] = this.createRow(p);
                    input.type = 'number';
                    input.value = defaultValue[p];
                    input.addEventListener('change', () =>
                    {
                        defaultValue[p] = parseFloat(input.value);
                        // assign a new object to replace the old one
                        this.game.config({ [prop.name]: Object.assign({}, defaultValue) });
                    });
                    div.appendChild(row);
                }
                break;
            default:
                const span = document.createElement('span');
                span.className = 'input';
                span.innerText = `Unhandled property type: ${prop.type}`;
                div.appendChild(span);
                break;
        }
        this.parent.appendChild(div);
    }

    private createRow(name: string)
    {
        const row = document.createElement('div');
        row.className = 'row';
        const label = document.createElement('label');
        label.innerText = name;
        row.appendChild(label);
        const input = document.createElement('input');
        row.appendChild(input);

        return [row, input] as [HTMLDivElement, HTMLInputElement];
    }

    private createStages(stages: StageOption[])
    {
        const div = document.createElement('div');
        div.className = 'prop';
        const label = document.createElement('label');
        label.innerText = 'Stage';
        div.appendChild(label);
        const select = document.createElement('select');
        select.className = 'input';
        for (let i = 0; i < stages.length; ++i)
        {
            const enumValue = stages[i];
            const opt = document.createElement('option');
            opt.value = enumValue.value.toString();
            opt.innerText = enumValue.name;
            select.appendChild(opt);
        }
        select.value = stages[0].value.toString();
        div.appendChild(select);

        return [div, select] as [HTMLDivElement, HTMLSelectElement];
    }
}
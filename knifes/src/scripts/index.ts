import {EndConfig, PixiGame, StandardEndings, Utils} from "@imblox/game-utils";
import {defaultConfig, IConfig} from "./constant/Config";
import {AppStage} from "./constant/AppStage";
import {WheelScene} from "./WheelScene";

export let mainGame: Game;

export class Game extends PixiGame<IConfig> {

    private scene: WheelScene;

    protected init() {
        super.init();
        mainGame = this;
        this.gameConfig = defaultConfig;
        this.scene = new WheelScene();
    }

    public async showEnding(ending: EndConfig, instant = true): Promise<void> {
        this.removeListener();
        await super.showEnding(ending, instant);
        this.scene.clear();
    }

    protected clear() {
        super.clear();
        this.removeListener();
        this.scene.clear();
    }

    private onStop = async (iswin: boolean) => {
        if (this.gameStage === AppStage.GAME) {
            await this.createWinScene(iswin? StandardEndings.Win: StandardEndings.Lose);
        }
    };

    async updateChanges(changes: Partial<IConfig>): Promise<void> {

        await super.updateChanges(changes);

        for (const [name, value] of Object.entries(changes)) {

            switch (name) {

                case "knifeColors":
                    await this.scene.restart();
                    break;
                case "wheelRotation":
                    this.scene.wheel.updateRotation();
                    break;

                case "wheelColors":
                case "wheelColorRadius":
                    this.scene.wheel.updateShape();
                    break;

                case "wheelImage":
                case "wheelRadius":
                    this.scene.wheel.updateImage();
                    break;
                case "obstacles":
                case "obstacleImage":
                    this.scene.wheel.updateObjects();
                    break;
                case "timeLimit":
                    this.scene.updateTimeLimit();
                    break;
                case "retries":
                    this.scene.lifeIndicator.updateLives();
                    break;
            }
        }

    }

    // @ts-ignore
    protected update(frame: number) {
        this.scene.update(frame / 60);
    }

    async applyStage() {
        await this.updateChanges(this.gameConfig);
        Utils.playSound(this.gameConfig.startSound);
        this.wrapper.addScene(this.scene);
        this.addListener();
    }

    private addListener() {
        this.removeListener();
        this.wrapper.interactive = true;
        this.wrapper.on("pointerdown", this.scene.touch, this.scene);
        this.scene.on("finish", this.onStop);
    }

    private removeListener() {
        this.wrapper.interactive = false;
        this.wrapper.off("pointerdown", this.scene.touch, this.scene);
        this.scene.off("finish", this.onStop);
    }
}

module.exports = {Game, AppStage};

import {Container, Graphics} from "pixi.js";
import {mainGame} from "./index";
import {Utils} from "@imblox/game-utils";
import {Back, gsap} from "gsap";

export class LifeIndicator extends Container {
    private lives: Graphics[] = [];
    private container: Container = new Container();

    constructor() {
        super();

        this.container.x = this.container.y = 25;
        this.addChild(this.container);

    }

    async removeOne() {

        let lastLife = this.lives.pop();

        if (lastLife) {
            await Utils.tweenAsing(gsap.to(lastLife.scale, {
                x: 0, y: 0,
                duration: 0.75,
                ease: Back.easeIn,
            }));

        }

    }

    get length() {
        return  this.lives.length;
    }

    destroy() {
        this.clear();
    }

    clear() {
        for (const live of this.lives) {
            live.destroy();
        }
        this.lives = [];
    }

    public updateLives() {

        this.clear();
        let nextY = 0;
        for (let n = 0; n < mainGame.gameConfig.retries!; n++) {
            let life = new Graphics();
            life.beginFill(0xaa0000);
            life.drawCircle(0, 0, 20);
            life.endFill();
            life.y = nextY;
            nextY += life.height + 5;
            this.container.addChild(life);
            this.lives.push(life);
        }
    }
}

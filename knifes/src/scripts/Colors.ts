export class Colors {
  static namedColors: any = {
    red: 0xff0000,
    green: 0x00ff00,
    blue: 0x0000ff,
    yellow: 0xffff00,
    neutral: 0xcccccc
  };

  static byName(colorName: string): number {
    return this.namedColors[colorName] || 0xcccccc;
  }
}

import {Container, Sprite} from "pixi.js";
import {Colors} from "./Colors";
import {mainGame} from "./index";

class Indicator extends Container {

    private sprite: Sprite;

    constructor(public color: string) {
        super();

        this.sprite = Sprite.from(mainGame.gameConfig.indicatorImage!);
        this.sprite.tint = Colors.byName(color);
        this.addChild(this.sprite);
    }

    public get height(): number {
        return this.sprite.height;
    }

    destroy() {
        this.sprite.destroy();
        super.destroy();
    }
}

export class KnifeIndicator extends Container {
    private indicators: Indicator[] = [];

    updateImages() {
        this.clearImages();

        if (!mainGame.gameConfig.knifeColors) {
            return;
        }

        if (typeof mainGame.gameConfig.knifeColors === 'number') {
            let l = mainGame.gameConfig.knifeColors;
            mainGame.gameConfig.knifeColors = [];
            for (let j = 0; j < l; j++) {
                mainGame.gameConfig.knifeColors.push('neutral');
            }
        }

        let nextY = 0;
        let i = 0;
        for (let j = mainGame.gameConfig.knifeColors.length - 1; j >= 0; j--) {
            let color = mainGame.gameConfig.knifeColors[j];
            let indicator = new Indicator(color);
            indicator.y = nextY;
            nextY -= indicator.height;
            this.addChild(indicator);
            this.indicators.unshift(indicator);
        }
    }

    get length() {
        return this.indicators.length;
    }

    pop(): string | null {
        const indicator = this.indicators.shift();
        if (indicator) {
            const color = indicator.color;
            indicator.destroy();
            return color;
        }
        return null;
    }

    private clearImages() {
        for (const indicator of this.indicators) {
            indicator.destroy();
        }
        this.indicators = [];
    }

    clear() {
        this.clearImages();
    }
}

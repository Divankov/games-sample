import {Container, Sprite, Texture} from "pixi.js";
import {mainGame} from "./index";

export class Knife extends Container {
    get color(): string {
        return this._color;
    }

    private sprite: Sprite;
    private _color: string;


    constructor() {
        super();
        this.sprite = new Sprite();
        this.addChild(this.sprite);
    }

    updateImage(color: string | null) {
        this._color = color || "neutral";

        const knifes = mainGame.gameConfig.knifeImages;
        if (color && knifes && knifes.length) {
            const knife = knifes.find((knife)=>knife.color === color) || knifes[0];
            this.sprite.texture = Texture.from(knife.image);
            this.sprite.visible = true;
        } else {
            this.sprite.visible = false;
        }
    }

    clear() {
        this.sprite.visible = false;
    }
}

import {Utils} from "@imblox/game-utils/src";
import {mainGame} from "./index";
import {Container, Graphics, Sprite, Texture} from "pixi.js";
import {Knife} from "./Knife";
import {Back, gsap} from "gsap";
import {Colors} from "./Colors";
import {IRotation} from "./constant/Config";

interface IObject {
    at: number,
    obj: Container,
    aw: number
}

export class Wheel extends Container {


    private hitLayer: Container = new Container();
    private wheelLayer: Container = new Container();
    private obstaclesLayer: Container = new Container();
    private wheelSprite: Sprite;
    public canHit: boolean = false;
    private _speed: number = 0;
    private shape: Graphics = new Graphics();
    private stuckObjects: IObject[] = [];
    private speedInTransition: boolean = false;
    private currentSpeedDuration: number = 0;
    private currentSpeedIndex: number = 0;

    constructor() {
        super();

        this.addChild(this.hitLayer);
        this.hitLayer.addChild(this.obstaclesLayer);
        this.addChild(this.wheelLayer);
        this.wheelLayer.addChild(this.shape);

        this.wheelSprite = new Sprite();
        this.wheelSprite.anchor.set(0.5, 0.5);
        this.wheelLayer.addChild(this.wheelSprite);
        this.canHit = true;
    }

    clearObjects() {
        for (const object of this.stuckObjects) {
            object.obj.destroy();
        }
        this.stuckObjects = [];
    }

    updateObjects() {
        this.clearObjects();

        this.obstaclesLayer.rotation = Math.PI / 2;


        if (!mainGame.gameConfig.obstacles || !mainGame.gameConfig.obstacleImage) {
            return;
        }

        for (let obstacle of mainGame.gameConfig.obstacles) {

            let rndAngle = Math.PI * obstacle / 180;
            let sprite = Sprite.from(mainGame.gameConfig.obstacleImage);
            sprite.anchor.set(0.5, 0.5);
            sprite.rotation = Math.PI * 2 - rndAngle;
            sprite.position.set(
                Math.cos(sprite.rotation) * this.colorRadius,
                Math.sin(sprite.rotation) * this.colorRadius
            );
            this.obstaclesLayer.addChild(sprite);

            let at = rndAngle;

            this.stuckObjects.push({
                at,
                obj: sprite,
                aw: (sprite.height / this.circumference) * (Math.PI * 2)
            });
        }
    }

    updateRotation() {
        this.currentSpeedDuration = 0;
        this.currentSpeedIndex = 0;
        this.speed = this.wheelRotation.speed;
    }

    updateImage() {
        if (mainGame.gameConfig.wheelImage) {
            this.wheelSprite.texture = Texture.from(mainGame.gameConfig.wheelImage);
            this.wheelSprite.width = this.wheelSprite.height = this.radius * 2;
            this.wheelSprite.visible = true;
        } else {
            this.wheelSprite.visible = false;
        }
    }

    get colors(): string[] {
        return mainGame.gameConfig.wheelColors || []
    }

    set speed(value: number) {
        this._speed = value;
    }

    get speed(): number {
        return this._speed;
    }

    get radius(): number {
        return mainGame.gameConfig.wheelRadius || 1;
    }

    get colorRadius(): number {
        return mainGame.gameConfig.wheelColorRadius || 1;
    }

    get sectorsNum(): number {
        return mainGame.gameConfig.wheelColors?.length || 1;
    }

    get circumference() {
        return Math.PI * this.radius * 2
    }

    getSector() {
        if (this.sectorsNum > 0) {
            let pointAt = Math.PI * 2 - this.rotation;
            while (pointAt > Math.PI * 2) {
                pointAt -= Math.PI * 2;
            }

            while (pointAt < 0) {
                pointAt += Math.PI * 2;
            }

            return Math.floor(pointAt / ((Math.PI * 2) / this.sectorsNum));

        } else {
            return 0;
        }
    }

    getSectorColor(sector: number) {

        if (sector < this.colors.length) {
            return this.colors[sector]
        } else {
            return "neutral";
        }
    }

    async hitWith(knife: Knife, cb: any) {
        Utils.playSound(mainGame.gameConfig.knifeThrowSound);
        await Utils.tweenAsing(gsap.to(knife, {
            y: this.y + this.colorRadius + knife.getBounds().height,
            duration: 0.11,
        }));

        const hitStuckObjects = this.stuckObjects.filter(stuck => {
            return (
                stuck.at > this.rotation - stuck.aw &&
                stuck.at < this.rotation + stuck.aw
            );
        });

        const sector = this.getSector();
        const sectorColor = this.getSectorColor(sector);
        if ((knife.color === "neutral" || knife.color === sectorColor || sectorColor==="neutral") && hitStuckObjects.length === 0) {
            this.stuckObjects.push({
                at: this.rotation,
                obj: knife,
                aw: (Math.min(knife.width, knife.height) / this.circumference) * (Math.PI * 2)
            });

            Utils.playSound(mainGame.gameConfig.knifeHitSound);
            cb(true, sector);

            knife.y -= knife.getBounds().height * 0.66;
            let position = this.toLocal(knife.position, this.parent);
            knife.x = position.x;
            knife.y = position.y;
            knife.rotation = -this.rotation - Math.PI / 2;
            this.hitLayer.addChild(knife);

            let originalY = this.y;
            const DEFLECTION_TIME = 0.05;

            await Utils.tweenAsing(gsap.to(this, {
                y: this.y - 10,
                duration: DEFLECTION_TIME / 2,

            }));

            await Utils.tweenAsing(gsap.to(this, {
                y: originalY,
                duration: DEFLECTION_TIME / 2,
                ease: Back.easeIn,
            }));

        } else {
            Utils.playSound(mainGame.gameConfig.knifeBounceSound);
            this.canHit = false;
            knife.pivot.set(knife.width / 2, knife.height / 2);
            await Utils.tweenAsing(gsap.to(knife, {
                y: 1000 * 2, x: knife.x + (Math.random()* 300 - 150) * 2, rotation: Math.PI * 2 * 2,
                duration: 1,
            }));
            knife.destroy();

            cb(false);
        }
    }


    get wheelRotationLength(): number {
        return mainGame.gameConfig.wheelRotation && mainGame.gameConfig.wheelRotation.length || 0;
    }

    get wheelRotation(): IRotation {
        if (this.wheelRotationLength > this.currentSpeedIndex) {
            return mainGame.gameConfig.wheelRotation![this.currentSpeedIndex];
        } else {
            return {speed: 10, acceleration: 0, duration: 1000000};
        }
    }

    async update(delta: number) {
        this.rotation += this.speed * (Math.PI * 2) * delta;

        while (this.rotation > Math.PI * 2) {
            this.rotation -= Math.PI * 2;
        }

        while (this.rotation < 0) {
            this.rotation += Math.PI * 2;
        }

        if (!this.speedInTransition) {
            if (this.currentSpeedDuration >= this.wheelRotation.duration) {
                this.currentSpeedIndex++;

                if (this.currentSpeedIndex >= this.wheelRotationLength) {
                    this.currentSpeedIndex = 0;
                }

                this.currentSpeedDuration = 0;
                this.speedInTransition = true;

                await Utils.tweenAsing(gsap.to(this,
                    {
                        speed: this.wheelRotation.speed,
                        duration: this.wheelRotation.acceleration
                    }));

                this.speedInTransition = false;
            } else {
                this.currentSpeedDuration += delta;
            }
        }

    }

    public updateShape() {
        this.shape.clear();
        if (this.colors.length) {
            let anglePerSector = (Math.PI * 2) / this.colors.length;
            for (let i = 0; i < this.colors.length; i++) {
                this.shape.beginFill(Colors.byName(this.colors[i]));
                this.shape.moveTo(0, 0);

                this.shape.arc(0, 0,
                    this.colorRadius,
                    anglePerSector * i,
                    anglePerSector * i + anglePerSector
                );
                this.shape.endFill();
            }
            this.shape.rotation = Math.PI / 2;
        } else {
            this.shape.beginFill(0xcccccc);
            this.shape.drawCircle(0, 0, this.colorRadius);
        }
        this.shape.endFill();

    }

    clear() {
        this.shape.clear();
        this.clearObjects();
        this.wheelSprite.visible = false;
    }
}



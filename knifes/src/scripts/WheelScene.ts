import {constant} from "./constant/Constant";
import {Scene, Utils} from "@imblox/game-utils";
import {Knife} from "./Knife";
import {Back, gsap} from "gsap";
import {Wheel} from "./Wheel";
import {Graphics} from "pixi.js";
import {KnifeIndicator} from "./KnifeIndicator";
import {LifeIndicator} from "./LifeIndicator";
import {mainGame} from "./index";
import TimeLimitIndicator from "./TimeLimitIndicator";

export class WheelScene extends Scene {

    public wheel: Wheel;
    public knife: Knife | null;
    public indicator: KnifeIndicator;
    public lifeIndicator: LifeIndicator;
    private timeLimit: number = 0;
    private timeLimitCurrent: number = 0;
    private timeLimitIndicator: TimeLimitIndicator;

    get width() {
        return constant.defaultSize.width;
    }

    get height() {
        return constant.defaultSize.height;
    }

    constructor() {
        super();

        this.sortableChildren = true;
        this.interactive = true;
        let bgGraphics = new Graphics();
        bgGraphics.beginFill(0x0, 0);
        bgGraphics.drawRect(0, 0, this.width, this.height);
        bgGraphics.endFill();
        this.addChild(bgGraphics);

        this.wheel = new Wheel();
        this.wheel.x = this.width / 2;
        this.wheel.y = this.height / 2 - 100;
        this.wheel.scale.set(0, 0);
        this.wheel.zIndex = 5;
        this.addChild(this.wheel);

        this.indicator = new KnifeIndicator();
        this.indicator.y = this.height - this.indicator.height;
        this.indicator.x = 10;
        this.addChild(this.indicator);

        this.timeLimitIndicator = new TimeLimitIndicator(this.width - 150);
        this.timeLimitIndicator.x = (this.width - this.timeLimitIndicator.width) / 2;
        this.timeLimitIndicator.y = 10;
        this.addChild(this.timeLimitIndicator);


        this.lifeIndicator = new LifeIndicator();
        this.lifeIndicator.y = 20;
        this.addChild(this.lifeIndicator);

    }

    async start() {
        this.wheel.scale.set(0, 0);
        gsap.to(this.wheel.scale, {
            x: 1, y: 1,
            duration: 0.3,
            ease: Back.easeOut,
        });

        await this.nextKnife();

    }

    async nextKnife() {

        this.knife = new Knife();
        this.knife.updateImage(this.indicator.pop());
        this.knife.pivot.set(15, 14);
        this.knife.rotation = -Math.PI / 2;
        this.knife.x = this.width / 2;
        this.knife.y = this.height + this.knife.getBounds().height;
        this.knife.alpha = 0;
        this.wheel.zIndex = 4;

        this.addChild(this.knife);

        this.wheel.canHit = false;
        await Utils.tweenAsing(gsap.to(this.knife, {
            y: this.height - 30, alpha: 1,
            duration: 0.005
        }));
        this.wheel.canHit = true;
    }

    touch() {
        if (this.wheel.canHit && this.knife) {
            this.wheel.hitWith(this.knife, this.onHit.bind(this));
            this.knife = null;
        }
    }

    onHit(success: boolean) {
        if (success) {
            if (this.indicator.length > 0) {
                this.nextKnife();
            } else {
                this.emit("finish", true);
            }
        } else {
            this.gameOver();
        }
    }

    updateTimeLimit() {
        this.timeLimit = mainGame.gameConfig.timeLimit || 0;
        this.timeLimitCurrent = mainGame.gameConfig.timeLimit || 0;
        this.timeLimitIndicator.visible = this.timeLimit > 0;

    }

    update(delta: number) {
        this.wheel.update(delta);
        if (this.timeLimit) {
            this.timeLimitCurrent = Math.max(0, this.timeLimitCurrent - delta);
            this.timeLimitIndicator.value = this.timeLimitCurrent / this.timeLimit;

            if (this.timeLimitCurrent == 0) {
                this.timeLimit = 0;
                this.gameOver();
            }
        }
    }


    async gameOver() {

        await Promise.all([
            this.lifeIndicator.removeOne(),
            Utils.tweenAsing(gsap.to(this.wheel.scale, {
                x: 0, y: 0,
                duration: 1,
                ease: Back.easeIn,
            }))
        ]);


        if (this.lifeIndicator.length) {
            await this.restart();
        } else {
            this.emit("finish", false);
        }

    }

    clear() {
        this.wheel.clear();
        if (this.knife) {
            this.knife.clear();
        }
        this.indicator.clear();
        this.lifeIndicator.clear();
        this.timeLimitIndicator.clear();


    }

    public async restart() {
        if (this.knife) {
            this.knife.clear();
        }
        this.wheel.updateObjects();
        this.indicator.updateImages();
        this.updateTimeLimit();
        await this.start();
    }
}

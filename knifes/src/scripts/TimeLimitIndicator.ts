import { Container, Graphics } from "pixi.js";

export default class TimeLimitIndicator extends Container {
  private bg: Graphics = new Graphics();
  private bar: Graphics = new Graphics();
  private barHeight: number = 0;
  private barWidth: number = 0;
  constructor(private indicatorWidth: number, private  indicatorHeight: number = 15) {
    super();


    this.bg.beginFill(0x444444);
    this.bg.drawRect(0, 0, indicatorWidth, indicatorHeight);
    this.bg.endFill();

    this.addChild(this.bg);

    this.barHeight = indicatorHeight - 4;
    this.barWidth = indicatorWidth - 4;

    this.bar = new Graphics();
    this.bar.beginFill(0xeeeeee);
    this.bar.drawRect(2, 2, this.barWidth, this.barHeight);
    this.bar.endFill();

    this.addChild(this.bar);
  }

  get width() {
    return this.indicatorWidth;
  }

  set value(newValue: number) {
    this.bar.width = this.barWidth * newValue;
    this.bar.x = (this.indicatorWidth - this.bar.width) / 2;
  }

  clear() {
      this.visible = false;
  }
}

# About this repository
This repository has been set up as a mono-repo, using [pnpm](https://pnpm.js.org/en/) to make installs faster and reduce the impact of all the `node_modules` folders on your hard drives.
It has not been set up with a full mono-repo management tool like Rush or Lerna, so you'll need to make sure you've built local dependencies. The top level scripts `build:all` or `build:all:prod` will do that for you, in addition to building all games.

# Notable packages
* `game-utils` (`@imblox/game-utils`) - Utility classes, methods, interfaces, and constants to build games with.
* `test-widget` (`@imblox/test-widget`) - Utility files for running a game locally without setting up a real Widget environment. In your webpack build process, copy the folder for the type of parent/canvas that your game needs to run.
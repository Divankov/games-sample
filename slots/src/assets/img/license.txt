The reel icons here (Bar3, bell, cherries, heart, horseshoe, lemon, Lucky7_rainbow, melon), are used under the CC-BY 3.0 license, and when distributed must be attributed to:
Molly "Cougarmint" Willits

The slot machine art itself is CC0 (public domain)
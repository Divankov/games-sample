import { randInt } from '@imblox/game-utils';
import { PayoutData } from './Config';

export class Payout
{
    private rows: string[][][];
    private payout: string;

    constructor(data: PayoutData)
    {
        this.payout = data.title;
        this.rows = data.names.map(arr => arr.split(' ').map(column => column.split(',')));
    }

    public getRandomWin(): string[]
    {
        const row = this.rows[randInt(0, this.rows.length - 1)];
        const out = [];
        for (let i = 0; i < row.length; ++i)
        {
            const column = row[i];
            out.push(column[randInt(0, column.length - 1)]);
        }
        return out;
    }

    public test(result: string[]): boolean
    {
        let found = false;
        for (let j = 0; j < this.rows.length; ++j)
        {
            const row = this.rows[j];
            for (let i = 0; i < result.length; ++i)
            {
                if (row[i].indexOf(result[i]) === -1)
                {
                    break;
                }
                if (i === row.length - 1)
                {
                    found = true;
                }
            }
            if (found)
            {
                return true;
            }
        }
        return false;
    }
}
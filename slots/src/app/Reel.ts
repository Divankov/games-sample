import { Container, Texture, TilingSprite } from 'pixi.js';
import { ExtPromise } from './PromiseUtils';
import TWEEN from "@tweenjs/tween.js";
import type { Machine } from './Machine';

/**
 * Spin speed is calculated by multiplying SPIN_SPEED by the symbol height
 * as 750px/second looks pretty good on a symbol height of 76
 */
const SPIN_SPEED = 750 / 76;
/**
 * Spin accel is double the spin speed.
 */
const SPIN_ACCEL = SPIN_SPEED * 2;

enum SpinState
{
    Stopped,
    SpinUp,
    Spinning,
    WaitingToSlow,
    Stopping
}

export class Reel extends Container {
    private reelTexture: Texture;
    private reelSprite: TilingSprite;
    private areaWidth: number;
    private areaHeight: number;
    private state: SpinState;
    private currentSpeed: number;
    private stopY: number;
    private stopPromise: ExtPromise<void>;
    private hitTargetPromise: ExtPromise<void>;
    private reelLength: number;
    private maxSpeed: number;
    private accel: number;
    private stopTween: any;
    private machine: Machine;
    public lastTargetIndex: number = -1;
    /** Height of largest symbol + symbol spacing */
    private _symbolHeight:number;
    private _speedMultiplier:number = 1;

    constructor(machine: Machine)
    {
        super();
        this.machine = machine;

        this.state = SpinState.Stopped;
        this.currentSpeed = 0;
        this.areaWidth = 100;
        this.areaHeight = 100;
        this.symbolHeight = 0;

        this.reelSprite = new TilingSprite(Texture.WHITE, this.areaWidth, this.areaHeight);
        this.addChild(this.reelSprite);
    }

    public set reelWidth(value: number)
    {
        const oldHeight = this.areaHeight;
        const oldScale = this.reelSprite.tileScale.y;
        const oldCenter = (this.reelSprite.tilePosition.y - this.reelLength - (oldHeight / 2)) / -oldScale;
        this.areaWidth = value;
        this.reelSprite.width = this.areaWidth;
        if (this.reelTexture)
        {
            this.reelSprite.tileScale.set(this.areaWidth / this.reelTexture.width);
            this.reelLength = this.reelTexture.height * this.reelSprite.tileScale.y;
            this.setReelY(oldCenter);
        }
    }

    public set reelHeight(value: number)
    {
        const oldHeight = this.areaHeight;
        const oldScale = this.reelSprite.tileScale.y;
        const oldCenter = (this.reelSprite.tilePosition.y - this.reelLength - (oldHeight / 2)) / -oldScale;
        this.areaHeight = value;
        this.reelSprite.height = this.areaHeight;
        this.setReelY(oldCenter);
    }

    public set texture(texture: Texture)
    {
        this.reelSprite.texture = this.reelTexture = texture;
        this.reelSprite.tileScale.set(this.areaWidth / texture.width);
        this.reelLength = this.reelTexture.height * this.reelSprite.tileScale.y;
    }

    public set symbolHeight(height: number)
    {
        this._symbolHeight = height;
        this.maxSpeed = height * SPIN_SPEED * this._speedMultiplier;
        this.accel = height * SPIN_ACCEL * this._speedMultiplier;
    }

    public set speedMultiplier(speed: number)
    {
        this._speedMultiplier = speed;
        this.maxSpeed = this._symbolHeight * SPIN_SPEED * this._speedMultiplier;
        this.accel = this._symbolHeight * SPIN_ACCEL * this._speedMultiplier;
    }

    /** Sets the reel's y position (the tile position) to be a specific symbol */
    public setReelY(value: number)
    {
        let pos = this.reelLength - (value * this.reelSprite.tileScale.y) + (this.areaHeight / 2);
        this.reelSprite.tilePosition.y = pos;
    }

    public cancel()
    {
        this.state = SpinState.Stopped;
        this.currentSpeed = 0;
        if (this.stopTween)
        {
            this.stopTween.stop();
            this.stopTween = null;
        }
    }

    public start()
    {
        this.state = SpinState.SpinUp;
        this.currentSpeed = 0;
        if (this.stopTween)
        {
            this.stopTween.stop();
            this.stopTween = null;
        }
    }

    public stop(targetY: number): [Promise<void>, Promise<void>]
    {
        this.stopY = this.reelLength - (targetY * this.reelSprite.tileScale.y) + (this.areaHeight / 2);
        this.state = SpinState.WaitingToSlow;
        this.stopPromise = new ExtPromise();
        this.hitTargetPromise = new ExtPromise();
        return [this.hitTargetPromise.promise, this.stopPromise.promise];
    }

    public update(elapsed:number)
    {
        if (!this.reelTexture || this.state === SpinState.Stopped) return;

        const delta = elapsed / 1000;
        let movement = 0;
        switch (this.state)
        {
            case SpinState.SpinUp:
                this.currentSpeed += delta * this.accel;
                if (this.currentSpeed > this.maxSpeed)
                {
                    this.currentSpeed = this.maxSpeed;
                    this.state = SpinState.Spinning;
                }
                movement = this.currentSpeed * delta;
                break;
            case SpinState.Spinning:
                movement = this.currentSpeed * delta;
                break;
            case SpinState.WaitingToSlow:
                movement = this.currentSpeed * delta;
                // start stopping a certain distance ahead based on our speed - if our movement would take us past that point then
                // then we start tweening with a bounce
                const stopPoint = (this.reelSprite.tilePosition.y + this.currentSpeed / 10) % this.reelLength;
                // stopY may be after the end of the reel, so account for that by doing two checks
                if (Math.abs(stopPoint - this.stopY) <= movement || Math.abs(stopPoint - (this.stopY - this.reelLength)) <= movement)
                {
                    movement = 0;
                    this.state = SpinState.Stopping;
                    const target = this.stopY < this.reelSprite.tilePosition.y ? this.stopY + this.reelLength : this.stopY;
                    this.stopTween = new TWEEN.Tween(this.reelSprite.tilePosition as any)
                        .to({y: target + this._symbolHeight / 3}, 250)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .start(undefined as any)
                        .chain(new TWEEN.Tween(this.reelSprite.tilePosition as any)
                            .to({y: target}, 500)
                            .easing(TWEEN.Easing.Back.Out)
                            .onComplete(() => {
                                this.state = SpinState.Stopped;
                                this.stopTween = null;
                                this.stopPromise.resolve();
                            })
                        );
                }
                break;
            case SpinState.Stopping:
                // track when we hit the target for audio purposes
                if (this.reelSprite.tilePosition.y >= this.stopY)
                {
                    this.hitTargetPromise.resolve();
                }
                break;
        }
        if (movement)
        {
            let pos = this.reelSprite.tilePosition.y + movement;
            if (pos > this.reelLength)
            {
                while (pos > this.reelLength)
                {
                    pos -= this.reelLength;
                }
            }
            this.reelSprite.tilePosition.y = pos;
        }
    }
}
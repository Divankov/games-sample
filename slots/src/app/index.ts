import TWEEN from "@tweenjs/tween.js";
import { Application, Texture, utils } from "pixi.js";
import { BaseGame, PixiEnding, StageParams, EndConfig, StandardEndings, Sound, IRect } from '@imblox/game-utils';
import { Machine } from "./Machine";
import { IConfig, defaultConfig, Stages } from './Config';
import { PayoutChart } from "./PayoutChart";
import { Payout } from "./Payout";
import { Strobe } from "./Strobe";
import { Menu } from './Menu';

export { defaultConfig, Properties } from './Config';

export default class Slots extends BaseGame<IConfig> {
    public readonly stages = [
        {name: 'Gameplay', value: Stages.GAMEPLAY},
        {name: 'Position Setup', value: Stages.PREVIEW_SETUP}
    ];

    private wrapper: HTMLDivElement;
    private app: Application;
    private machine: Machine;
    private strobe: Strobe;
    private menu: Menu;
    private chart: PayoutChart;
    private ending: PixiEnding;
    private payouts: Payout[];
    private _stage: Stages;
    private bgMusic: Sound;
    private payoutPreviewCB?: (rect: IRect)=>void;
    private spinPreviewCB?: (rect: IRect) => void;
    private reelPreviewCB?: (data: {
        reelRectWidth: number,
        reelRectHeight: number,
        reelCenterY: number,
        reelCentersX: number[]
    }) => void;

    /**
     * Game constructor
     */
    constructor(options: { parent: HTMLDivElement })
    {
        super(options);

        this._stage = Stages.GAMEPLAY;

        this.wrapper = document.createElement('div');
        this.wrapper.style.width = '100%';
        this.wrapper.style.height = '100%';
        this.wrapper.style.position = 'relative';
        this.parent.appendChild(this.wrapper);

        const canvas = document.createElement('canvas');
        canvas.style.position = 'absolute';
        canvas.style.left = '0px';
        canvas.width = 200;
        canvas.height = 200;

        this.wrapper.appendChild(canvas);

        utils.skipHello();
        this.app = new Application({
            view: canvas,
            transparent: true
        });

        this.strobe = new Strobe();
        this.app.stage.addChild(this.strobe);

        this.machine = new Machine();
        this.machine.setStage(this._stage);
        this.app.stage.addChild(this.machine);
        this.machine.on('result', this.handleResult, this);
        this.machine.on('payout', () => {
            if (this._stage !== Stages.GAMEPLAY) return;
            this.chart.show();
        });
        this.machine.on('spin', () => {
            if (this._stage !== Stages.GAMEPLAY) return;
            this.machine.enabled = false;
            this.machine.spin();
            if (this.currentProps.strobeWhenSpinning)
            {
                this.strobe.start();
            }
        });

        this.menu = new Menu(defaultConfig.controlColor, Sound.toggleSound);
        this.app.stage.addChild(this.menu);

        this.bgMusic = new Sound();

        this.ending = new PixiEnding(this.app, () => {
            this.machine.visible = false;
            this.menu.visible = false;
        });

        this.chart = new PayoutChart(this.wrapper);
    }

    private start()
    {
        this.app.ticker.add(this.update, this);
        this.machine.enabled = true;
        this.machine.visible = true;
        this.menu.visible = true;
        this.bgMusic.loop();
    }

    private stop()
    {
        this.app.ticker.remove(this.update, this);
        this.bgMusic.stop();
    }

    private update() {
        TWEEN.update(undefined as any);
        this.machine.update(this.app.ticker.elapsedMS);
        this.strobe.update(this.app.ticker.elapsedMS);
    }

    private handleResult(result: [string, string, string])
    {
        this.strobe.stop();
        this.chart.hide();
        for (let i = 0; i < this.payouts.length; ++i)
        {
            if (this.payouts[i].test(result))
            {
                this.stop();
                this.ending.show(
                    // get 0 id ending of the payout index
                    this.getEndConfig(StandardEndings.Win, i)!,
                    this.endCallback
                );
                return;
            }
        }
        this.stop();
        this.ending.show(
            // get the loss ending
            this.getEndConfig(StandardEndings.Lose)!,
            this.endCallback
        );
    }

    private async setReelLoopSound(url: string|null)
    {
        await this.machine.spinSound.load(url);
    }

    private async setReelStopSound(url: string|null)
    {
        await this.machine.stopSound.load(url);
    }

    public set randomSpinFunc(func: (() => Promise<number|string[]>) | null)
    {
        if (!func)
        {
            this.machine.randomSpinFunc = null;
            return;
        }

        this.machine.randomSpinFunc = async () => {
            const index = await func();
            if (typeof index === 'number' && index >= 0 && index < this.payouts.length)
            {
                return this.payouts[index].getRandomWin();
            }
            if (Array.isArray(index))
            {
                return index;
            }
            let valid = false;
            let result: string[];
            while(!valid)
            {
                result = [this.machine.getRandomSymbol(), this.machine.getRandomSymbol(), this.machine.getRandomSymbol()];
                valid = !this.payouts.some(p => p.test(result));
            }
            return result!;
        };
    }

    public gameSpaceRectToMachineSpace(rect: IRect): IRect
    {
        const out = { x: 0, y: 0, width: rect.width * this.machine.scale.x, height: rect.height * this.machine.scale.y };
        this.machine.toLocal(rect, undefined, out as any);
        return out;
    }

    async config(config:Partial<IConfig>) {
        const tasks: Promise<any>[] = [];
        let startGame = false;
        if (!this.currentProps)
        {
            // if initial config, use default properties
            config = this.getChangesAndUpdateBackground(Object.assign({}, defaultConfig, config));
            startGame = true;
        }
        else
        {
            // otherwise record changes
            config = this.getChangesAndUpdateBackground(config);
        }

        if (config.hasOwnProperty('isMuted'))
        {
            this.menu.setMute(!!config.isMuted);
            Sound.isMuted = !!config.isMuted;
        }

        let texturesToDestroy: Texture[] = [];
        let needsResize = false;
        if (config.hasOwnProperty('machineImage'))
        {
            if (config.machineImage)
            {
                texturesToDestroy.push(this.machine.texture);
                tasks.push(Texture.fromURL(config.machineImage!).then(tex => {
                    this.machine.texture = tex;
                    // if the image was changed to a new value and none of the layout values changed, reset the layout values
                    if (!config.hasOwnProperty('reelCenterY') && !config.hasOwnProperty('reelCentersX') &&
                        !config.hasOwnProperty('reelRectWidth') && !config.hasOwnProperty('reelRectHeight') &&
                        !config.hasOwnProperty('spinButton') && !config.hasOwnProperty('payoutButton'))
                    {
                        const { width, height } = tex;
                        setTimeout(() => {
                            this.payoutPreviewCB?.({x: width * 0.2, y: height * 0.8, width: width * 0.25, height: height * 0.1});
                            this.spinPreviewCB?.({ x: width * 0.55, y: height * 0.8, width: width * 0.25, height: height * 0.1 });
                            this.reelPreviewCB?.({
                                reelCenterY: height * 0.4,
                                reelCentersX: [width * 0.20, width * 0.5, width * 0.80],
                                reelRectWidth: width * 0.25,
                                reelRectHeight: width * 0.25,
                            });
                        }, 5);
                    }
                }));
                this.machine.visible = true;
                needsResize = true;
            }
            else
            {
                this.machine.visible = false;
            }
        }
        if (config.hasOwnProperty('reels'))
        {
            if (config.reels)
            {
                tasks.push(this.machine.handleSymbolChange(config.reels));
                this.chart.symbols = config.reels;
            }
        }
        if (config.hasOwnProperty('reelLoopSound'))
        {
            tasks.push(this.setReelLoopSound(config.reelLoopSound!));
        }
        if (config.hasOwnProperty('reelStopSound'))
        {
            tasks.push(this.setReelStopSound(config.reelStopSound!));
        }
        if (this.currentProps.payoutTableType === 'image' && this.currentProps.payoutTableImage)
        {
            tasks.push(this.chart.setImage(this.currentProps.payoutTableImage));
        }
        else if (this.currentProps.payoutTableType === 'generated' && this.currentProps.prizes)
        {
            this.chart.setData(this.currentProps.prizes);
        }

        if (config.endings)
        {
            tasks.push(this.ending.updateEnding(config.endings));
        }

        tasks.push(this.bgMusic.load(this.currentProps.backgroundMusic).then(() => this.bgMusic.loop()));

        if (tasks.length)
        {
            await Promise.all(tasks);
        }

        if (config.hasOwnProperty('controlColor'))
        {
            this.menu.setColor(config.controlColor || defaultConfig.controlColor);
        }

        if (config.hasOwnProperty('spinButton'))
        {
            this.machine.spinRect = config.spinButton!;
        }
        if (config.hasOwnProperty('payoutButton'))
        {
            this.machine.payoutRect = config.payoutButton!;
        }
        if (config.reelCenterY)
        {
            this.machine.reelY = config.reelCenterY;
        }
        if (config.reelRectWidth)
        {
            this.machine.reelWidth = config.reelRectWidth;
        }
        if (config.reelRectHeight)
        {
            this.machine.reelHeight = config.reelRectHeight;
        }
        if (config.reelCentersX)
        {
            this.machine.reelX = config.reelCentersX;
        }
        if (config.hasOwnProperty('symbolSpacing'))
        {
            this.machine.symbolSpacing = config.symbolSpacing || 0;
        }
        if (config.spinTime)
        {
            this.machine.spinTime = config.spinTime;
        }
        if (config.speedMultiplier)
        {
            this.machine.speedMultiplier = config.speedMultiplier;
        }
        if (config.prizes)
        {
            this.payouts = config.prizes.map(p => new Payout(p));
        }
        if (config.endings) {
            this.setEndings(config.endings);
        }

        if (needsResize)
        {
            this.resize(this.parent.clientWidth, this.parent.clientHeight);
        }
        for (const tex of texturesToDestroy)
        {
            tex.destroy(true);
        }
        if (startGame)
        {
            this.start();
        }
    }

    resize(width: number, height: number)
    {
        width = Math.floor(width);
        height = Math.floor(height);
        this.app.renderer.resize(width, height);
        this.strobe.width = width;
        this.strobe.height = height;
        this.menu.x = 10;// (width - this.menu.menuWidth) / 2;
        this.menu.y = height - this.menu.menuHeight - 10;
        // resize machine
        this.machine.resize(width, height - this.menu.menuHeight - 20);
        // resize ending
        this.ending.resize();
    }

    public async stage({ config, stage }: StageParams<IConfig>)
    {
        if (this.ending.showing)
        {
            this.hideEnding();
        }
        this._stage = stage;
        Sound.devMode = stage !== Stages.GAMEPLAY;
        await this.config(config);
        if (this.machine.setStage(stage))
        {
            // hook up listeners
            this.machine.payoutPreview!.on('resize', (rect: IRect) => {
                this.payoutPreviewCB?.(rect);
            });
            this.machine.spinPreview!.on('resize', (rect: IRect) =>
            {
                this.spinPreviewCB?.(rect);
            });
            for (let i = 0; i < this.currentProps.reelCentersX.length; ++i)
            {
                this.machine.reelPreview![i].on('resize', (rect: IRect) => {
                    const centers = this.currentProps.reelCentersX.slice();
                    centers[i] = rect.x + rect.width / 2;
                    this.reelPreviewCB?.({
                        reelCenterY: rect.y + rect.height / 2,
                        reelRectHeight: rect.height,
                        reelRectWidth: rect.width,
                        reelCentersX: centers,
                    });
                });
            }
        }
    }

    public onPayoutRectChange(cb: (rect:IRect)=>void): void
    {
        this.payoutPreviewCB = cb;
    }

    public onSpinRectChange(cb: (rect:IRect)=>void): void
    {
        this.spinPreviewCB = cb;
    }

    public onReelLayoutChange(cb: (data: {
        reelRectWidth: number,
        reelRectHeight: number,
        reelCenterY: number,
        reelCentersX: number[]
    }) => void): void
    {
        this.reelPreviewCB = cb;
    }

    public restart(): void
    {
        // just to be safe, clean up listeners
        this.ending.hide();
        this.chart.hide();
        this.strobe.stop();
        this.stop();
        this.machine.stop();
        // reattach listeners
        this.start();
    }

    public async showEnding(end: EndConfig)
    {
        this.chart.hide();
        this.machine.stop();
        this.stop();
        await this.ending.show(
            end,
            this.endCallback,
            true
        );
    }

    public hideEnding()
    {
        this.ending.hide();
        this.restart();
    }
}

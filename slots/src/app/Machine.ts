import { Container, Sprite, Texture, Rectangle, WRAP_MODES, Point, InteractionEvent } from 'pixi.js';
import { Reel } from './Reel';
import { SymbolData, Stages } from './Config';
import { wait } from './PromiseUtils';
import { randInt, IRect, ResizableRect, Sound } from '@imblox/game-utils';

function loadImage(url: string)
{
    return new Promise<HTMLImageElement>(resolve =>
    {
        const img = new Image();
        img.crossOrigin = 'anonymous';
        img.onload = () =>
        {
            resolve(img);
        };
        img.src = url;
    });
}

interface LoadedSymbol
{
    name: string;
    image: HTMLImageElement;
    weight: number;
    centerY: number;
}

export class Machine extends Container
{
    private bg: Sprite;
    private payoutButton: Container;
    private spinButton: Container;
    private reels: Reel[];
    private reelTexture: Texture;
    private symbols: LoadedSymbol[]|null = null;
    private totalSymbolWeight: number;
    private reelWindow = new Rectangle(0, 0, 0, 0);
    private reelCenters = [new Point(0, 0), new Point(0, 0), new Point(0, 0)];
    private spinFunc: (() => Promise<string[]>)|null;
    public spinSound: Sound;
    public stopSound: Sound;
    private spinning: boolean;
    private stopDelay: number;
    public payoutPreview: ResizableRect|null;
    public spinPreview: ResizableRect|null;
    public reelPreview: ResizableRect[]|null;
    private _stage: Stages;
    private _symbolSpacing: number = NaN;

    constructor()
    {
        super();

        this.spinSound = new Sound();
        this.stopSound = new Sound();

        this.bg = new Sprite();
        this.addChild(this.bg);

        this.reels = [new Reel(this), new Reel(this), new Reel(this)];
        for (const reel of this.reels)
        {
            this.addChild(reel);
        }

        this.payoutButton = new Container();
        this.payoutButton.hitArea = new Rectangle();
        this.payoutButton.on('pointertap', (ev: InteractionEvent) => {
            if (ev.data.button !== 0) return;
            this.emit('payout');
        });
        this.payoutButton.interactive = true;
        this.payoutButton.cursor = 'pointer';
        this.payoutButton.renderable = false;
        this.addChild(this.payoutButton);

        this.spinButton = new Container();
        this.spinButton.hitArea = new Rectangle();
        this.spinButton.on('pointertap', (ev: InteractionEvent) => {
            if (ev.data.button !== 0) return;
            this.emit('spin');
        });
        this.spinButton.interactive = true;
        this.spinButton.cursor = 'pointer';
        this.spinButton.renderable = false;
        this.addChild(this.spinButton);

        this.spinning = false;
    }

    public set enabled(enable: boolean)
    {
        this.spinButton.interactive = this.payoutButton.interactive = enable;
    }

    public setStage(stage: Stages)
    {
        this._stage = stage;
        if (stage === Stages.PREVIEW_SETUP)
        {
            this.enabled = false;
            if (!this.payoutPreview)
            {
                this.payoutPreview = new ResizableRect(0x8888ff, 'assets/img/trophy.png');
                this.payoutPreview.resize(this.payoutButton.x, this.payoutButton.y,
                    (this.payoutButton.hitArea as Rectangle).width, (this.payoutButton.hitArea as Rectangle).height);
                this.addChild(this.payoutPreview);
                this.spinPreview = new ResizableRect(0xff0000, 'assets/img/return.png');
                this.spinPreview.resize(this.spinButton.x, this.spinButton.y,
                    (this.spinButton.hitArea as Rectangle).width, (this.spinButton.hitArea as Rectangle).height);
                this.addChild(this.spinPreview);
                this.reelPreview = [];
                for (let i = 0; i < this.reelCenters.length; ++i)
                {
                    const center = this.reelCenters[i];
                    const preview = new ResizableRect(0x00ff00, `assets/img/button${i+1}.png`);
                    preview.resize(center.x - this.reelWindow.width / 2, center.y - this.reelWindow.height / 2,
                        this.reelWindow.width, this.reelWindow.height);
                    this.addChild(preview);
                    this.reelPreview.push(preview);
                }
                return true;
            }
        }
        else if (this.payoutPreview)
        {
            this.enabled = true;
            for (const preview of [this.payoutPreview, this.spinPreview!, ...this.reelPreview!])
            {
                this.removeChild(preview);
                preview.destroy({children: true});
            }
            this.payoutPreview = null;
            this.spinPreview = null;
            this.reelPreview = null;
        }
        return false;
    }

    public set randomSpinFunc(func: (() => Promise<string[]>)|null)
    {
        this.spinFunc = func;
    }

    public set spinTime(value: number)
    {
        this.stopDelay = value * 1000;
    }

    public set payoutRect(rect: IRect|null)
    {
        this.payoutButton.position.copyFrom(rect || { x: 0, y: 0 });
        const hitRect = this.payoutButton.hitArea as Rectangle;
        hitRect.width = rect?.width || 0;
        hitRect.height = rect?.height || 0;
        this.payoutPreview?.resize(this.payoutButton.x, this.payoutButton.y, hitRect.width, hitRect.height);
    }

    public set spinRect(rect: IRect|null)
    {
        this.spinButton.position.copyFrom(rect || { x: 0, y: 0 });
        const hitRect = this.spinButton.hitArea as Rectangle;
        hitRect.width = rect?.width || 0;
        hitRect.height = rect?.height || 0;
        this.spinPreview?.resize(this.spinButton.x, this.spinButton.y, hitRect.width, hitRect.height);
    }

    public set reelWidth(value: number)
    {
        this.reelWindow.width = value;
        for (let i = 0; i < this.reels.length; ++i)
        {
            const reel = this.reels[i];
            const center = this.reelCenters[i];
            reel.x = center.x - this.reelWindow.width / 2;
            reel.reelWidth = value;
            this.reelPreview?.[i].resize(center.x - this.reelWindow.width / 2, center.y - this.reelWindow.height / 2,
                this.reelWindow.width, this.reelWindow.height);
        }
    }

    public set reelHeight(value: number)
    {
        this.reelWindow.height = value;
        for (let i = 0; i < this.reels.length; ++i)
        {
            const reel = this.reels[i];
            const center = this.reelCenters[i];
            reel.y = center.y - this.reelWindow.height / 2;
            reel.reelHeight = value;
            if (reel.lastTargetIndex > -1)
            {
                reel.setReelY(this.symbols![reel.lastTargetIndex].centerY);
            }
            this.reelPreview?.[i].resize(center.x - this.reelWindow.width / 2, center.y - this.reelWindow.height / 2,
                this.reelWindow.width, this.reelWindow.height);
        }
    }

    public set reelY(value: number)
    {
        for (let i = 0; i < this.reels.length; ++i)
        {
            const center = this.reelCenters[i];
            center.y = value;
            this.reels[i].y = value - this.reelWindow.height / 2;
            this.reelPreview?.[i].resize(center.x - this.reelWindow.width / 2, center.y - this.reelWindow.height / 2,
                this.reelWindow.width, this.reelWindow.height);
        }
    }

    public set reelX(value: number[])
    {
        for (let i = 0; i < this.reels.length; ++i)
        {
            const center = this.reelCenters[i];
            center.x = value[i];
            this.reels[i].x = value[i] - this.reelWindow.width / 2;
            this.reelPreview?.[i].resize(center.x - this.reelWindow.width / 2, center.y - this.reelWindow.height / 2,
                this.reelWindow.width, this.reelWindow.height);
        }
    }

    public get texture() { return this.bg.texture; }

    public set texture(texture: Texture)
    {
        this.bg.texture = texture;
    }

    public set symbolSpacing(spacing: number)
    {
        this._symbolSpacing = spacing;
        this.redrawReelTexture();
    }

    public set speedMultiplier(speed: number)
    {
        for (let i = 0; i < this.reels.length; ++i)
        {
            this.reels[i].speedMultiplier = speed;
        }
    }

    private didSymbolImagesChange(incoming: SymbolData[])
    {
        if (!incoming || !this.symbols || incoming.length !== this.symbols.length)
        {
            return true;
        }
        for (let i = 0; i < incoming.length; ++i)
        {
            if (incoming[i].src !== this.symbols[i].image.src)
            {
                return true;
            }
        }
        return false;
    }

    public async handleSymbolChange(symbols: SymbolData[])
    {
        if (this.didSymbolImagesChange(symbols))
        {
            await this.updateReelTexture(symbols);
        }
        else
        {
            for (let i = 0; i < symbols.length; ++i)
            {
                this.symbols![i].name = symbols[i].name;
                this.symbols![i].weight = symbols[i].weight;
            }
        }
    }

    private async updateReelTexture(symbols: SymbolData[]) {
        this.symbols = await Promise.all(
        symbols.map(async (symbol) => {
            const image = await loadImage(symbol.src)
            return { image, weight: symbol.weight, name: symbol.name, centerY: 0 };
        })
        );
        this.redrawReelTexture();
    }

    private redrawReelTexture()
    {
        if (!this.symbols || isNaN(this._symbolSpacing)) return;

        this.totalSymbolWeight = 0;
        let maxHeight = 0;
        let maxWidth = 0;
        for (const symbol of this.symbols)
        {
            const image = symbol.image;
            if (image.width > maxWidth)
            {
                maxWidth = image.width;
            }
            if (image.height > maxHeight)
            {
                maxHeight = image.height;
            }
        }
        const canvas = document.createElement('canvas');
        canvas.width = maxWidth;
        canvas.height = (maxHeight + this._symbolSpacing) * this.symbols.length;
        const context = canvas.getContext('2d')!;
        let nextY = 0;
        for (const symbol of this.symbols)
        {
            const { image } = symbol;
            const imageWidth = image.width;
            const imageHeight = image.height;
            const yPos = Math.floor((maxHeight - imageHeight) / 2 + nextY);
            context.drawImage(
                symbol.image,
                Math.floor((maxWidth - imageWidth) / 2),// x pos
                yPos,// y pos
                imageWidth,// destination width
                imageHeight// destination height
            );
            symbol.centerY = yPos + imageHeight / 2;
            nextY += this._symbolSpacing + maxHeight;

            this.totalSymbolWeight += symbol.weight;
        }
        const old = this.reelTexture;
        this.reelTexture = Texture.from(canvas);
        this.reelTexture.baseTexture.wrapMode = WRAP_MODES.REPEAT;

        for (const reel of this.reels)
        {
            reel.texture = this.reelTexture;
            reel.symbolHeight = maxHeight + this._symbolSpacing;
            if (reel.lastTargetIndex === -1 || reel.lastTargetIndex >= this.symbols.length)
            {
                reel.lastTargetIndex = randInt(0, this.symbols.length - 1);
            }
            reel.setReelY(this.symbols[reel.lastTargetIndex].centerY);
        }
        old?.destroy(true);
    }

    public update(elapsed: number)
    {
        for (const reel of this.reels)
        {
            reel.update(elapsed);
        }
    }

    public getRandomSymbol()
    {
        if (!this.symbols) throw new Error('No symbols!');
        const rand = randInt(0, this.totalSymbolWeight);
        let runningTotal = 0;
        for (let i = 0; i < this.symbols.length; ++i)
        {
            runningTotal += this.symbols[i].weight;
            if (rand <= runningTotal)
            {
                return this.symbols[i].name;
            }
        }
        throw new Error('Unable to get random symbol!');
    }

    public async spin()
    {
        if (this.spinning) return;

        this.spinning = true;
        this.spinSound.loop();
        for (const reel of this.reels)
        {
            reel.start();
        }

        let result: string[];
        if (this.spinFunc)
        {
            [result] = await Promise.all([this.spinFunc(), wait(this.stopDelay)]);
        }
        else
        {
            result = [this.getRandomSymbol(), this.getRandomSymbol(), this.getRandomSymbol()];
            await wait(this.stopDelay);
        }
        for (let i = 0; i < this.reels.length; ++i)
        {
            await wait(500);
            const symbol = this.symbols!.find(s => s.name === result[i])!;
            const [hitTarget, stopped] = this.reels[i].stop(symbol.centerY);
            await hitTarget;
            this.stopSound?.play();
            if (i === this.reels.length - 1)
            {
                this.spinSound?.stop();
            }
            await stopped;
        }
        this.spinning = false;
        this.emit('result', result);
    }

    public stop()
    {
        this.spinSound?.stop();
        for (let i = 0; i < this.reels.length; ++i)
        {
            this.reels[i].cancel();
        }
        this.spinning = false;
    }

    public resize(width: number, height: number)
    {
        const scale = Math.min(width / this.texture.width, height / this.texture.height);

        this.scale.set(scale);
        this.position.set((width - this.width) / 2, (height - this.height) / 2);
    }
}
import { randInt } from "@imblox/game-utils";
import { Sprite, Texture } from "pixi.js";

const COLOR_TIME = 16 * 4;//4 frames at 16 milliseconds per frame (60fps)

export class Strobe extends Sprite
{
    private running:boolean;
    private timer:number;

    constructor()
    {
        super(Texture.WHITE);
        this.running = false;
        this.timer = 0;
        this.visible = false;
    }

    public start()
    {
        this.visible = true;
        this.running = true;
        this.timer = COLOR_TIME;
        this.tint = randInt(0, 0xffffff);
    }

    public update(elapsedMS:number)
    {
        if (!this.running) return;
        if ((this.timer -= elapsedMS) <= 0)
        {
            this.tint = randInt(0, 0xffffff);
            this.timer = COLOR_TIME;
        }
    }

    public stop()
    {
        this.visible = false;
        this.running = false;
    }
}
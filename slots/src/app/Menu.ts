import { Container, Sprite, Texture, utils } from "pixi.js";

const UI_SIZE = 25;

export class Menu extends Container
{
    private soundButton: Sprite;
    private soundOn: Texture;
    private soundOff: Texture;
    public menuWidth: number;
    public menuHeight: number;

    constructor(color: string, soundCB:()=>void)
    {
        super();

        const c = utils.string2hex(color);

        this.soundOn = Texture.from('assets/img/audioOn.png');
        this.soundOff = Texture.from('assets/img/audioOff.png');

        this.soundButton = new Sprite(this.soundOn);
        this.soundButton.interactive = true;
        this.soundButton.cursor = 'pointer';
        this.soundButton.on('pointertap', () => {
            this.soundButton.texture = this.soundButton.texture === this.soundOn ? this.soundOff : this.soundOn;
            soundCB();
        });
        this.soundButton.tint = c;
        this.soundButton.width = UI_SIZE;
        this.soundButton.height = UI_SIZE;
        this.soundButton.x = 0;
        this.soundButton.y = 0;
        this.addChild(this.soundButton);

        this.menuWidth = this.soundButton.x + this.soundButton.width;
        this.menuHeight = this.soundButton.height;
    }

    public setMute(muted:boolean)
    {
        if (muted)
        {
            this.soundButton.texture = this.soundOff;
        }
        else
        {
            this.soundButton.texture = this.soundOn;
        }
    }

    public setColor(color:string)
    {
        const c = utils.string2hex(color);
        this.soundButton.tint = c;
    }

    public hide()
    {
        this.visible = false;
    }

    public show() {
        this.visible = true;
    }
}
import { BaseGameConfig, PropertyList, StandardEndings, IRect } from '@imblox/game-utils';

export enum Stages {
    GAMEPLAY = 0,
    PREVIEW_SETUP = 1,
}

export interface SymbolData {
    src: string;
    name: string;
    weight: number;
}

export interface PayoutData {
    /**
     * Each name is a mutually exclusive combination of reel symbols, separated by spaces: "lemon lemon lemon" for example.
     * Each column can be a comma separated list of options though: "lemon,banana lemon,banana lemon,banana" for example.
     */
    names: string[];
    title: string;
    payoutImage?: string;
}

export interface IConfig extends BaseGameConfig {
    backgroundTileType: 'color' | 'image';
    /** game background color */
    backgroundTileColor: string;
    /** Game background image (CSS repeat) */
    backgroundTileImage: string | null;
    machineImage: string;
    payoutButton: IRect;
    spinButton: IRect;
    reelRectWidth: number;
    reelRectHeight: number;
    reelCenterY: number;
    reelCentersX: number[];
    reels: SymbolData[];
    symbolSpacing: number;
    reelStopSound: string;
    reelLoopSound: string;
    prizes: PayoutData[];
    payoutTableType: 'image' | 'generated'
    payoutTableImage: string;
    spinTime: number;
    speedMultiplier: number;
    strobeWhenSpinning: boolean;
    backgroundMusic: string;
    isMuted: boolean;
    /** game controls color */
    controlColor: string;
}

export const defaultConfig: IConfig = {
    backgroundTileType: 'color',
    backgroundTileColor: '#ffffff',
    backgroundTileImage: "assets/img/background.jpg",
    machineImage: "assets/img/SMFrame_blank.png",
    payoutButton: {
        x: 11,
        y: 83,
        width: 97,
        height: 25
    },
    spinButton: {
        x: 121,
        y: 83,
        width: 97,
        height: 25
    },
    backgroundMusic: 'assets/snd/soundtrack.mp3',
    isMuted: false,
    controlColor: '#0000ff',
    reelStopSound: 'assets/snd/symbol.mp3',
    reelLoopSound: 'assets/snd/reel.mp3',
    spinTime: 1,
    speedMultiplier: 1,
    reelRectWidth: 67,
    reelRectHeight: 69,
    reelCenterY: 6 + 69/2,
    reelCentersX: [7+67/2, 81+67/2, 155+67/2],
    strobeWhenSpinning: true,
    symbolSpacing: 10,
    reels: [
        { src: "assets/img/Lucky7_rainbow.png", name: "seven", weight: 1 },
        { src: "assets/img/Bar3.png", name: "bar", weight: 1 },
        { src: "assets/img/bell.png", name: "bell", weight: 1 },
        { src: "assets/img/cherries.png", name: "cherry", weight: 1 },
        { src: "assets/img/heart.png", name: "heart", weight: 1 },
        { src: "assets/img/horseshoe.png", name: "horseshoe", weight: 1 },
        { src: "assets/img/lemon.png", name: "lemon", weight: 1 },
        { src: "assets/img/melon.png", name: "melon", weight: 1 }
    ],
    prizes: [
        {
            names: ["seven seven seven"],
            title: "100"
        },
        {
            names: ["bar bar bar"],
            title: "2"
        },
        {
            names: ["bell bell bell"],
            title: "5"
        },
        {
            names: ["cherry cherry cherry"],
            title: "20"
        },
        {
            names: ["heart heart heart"],
            title: "50"
        },
        {
            names: ["horseshoe horseshoe horseshoe"],
            title: "40"
        },
        {
            names: ["lemon lemon lemon"],
            title: "10"
        },
        {
            names: ["melon melon melon"],
            title: "30"
        },
        {
            names: ["lemon melon cherry"],
            title: "1"
        }
    ],
    payoutTableType: 'generated',
    payoutTableImage: '',
    endings: [
        {
            id: -1,
            image: 'assets/img/lost.png',
            sound: 'assets/snd/lost.mp3'
        },
        {
            id: 0,
            image: 'assets/img/won.png',
            sound: 'assets/snd/win.mp3'
        }
    ]
};

export const Properties: PropertyList<IConfig> = [
    {
        name: 'backgroundTileType',
        title: 'Background Type',
        type: 'enum',
        enumValues: [
            { value: 'color', title: 'Color' },
            { value: 'image', title: 'Tiled Image' }
        ]
    },
    {
        name: 'backgroundTileColor',
        title: 'Background Color',
        type: 'color'
    },
    {
        name: 'backgroundTileImage',
        title: 'Background Image',
        type: 'image'
    },
    {
        name: 'reelLoopSound',
        title: 'Spin Sound Loop',
        type: 'sound'
    },
    {
        name: 'reelStopSound',
        title: 'Spin Stop Sound',
        type: 'sound'
    },
    {
        name: 'machineImage',
        title: 'Slot Machine Image',
        type: 'image'
    },
    {
        name: 'spinButton',
        title: 'Spin Button Image Area',
        type: 'rect'
    },
    {
        name: 'payoutButton',
        title: 'Payout Chart Button Image Area',
        type: 'rect'
    },
    {
        name: 'reelRectWidth',
        title: 'Reel Display Width',
        type: 'number'
    },
    {
        name: 'reelRectHeight',
        title: 'Reel Display Height',
        type: 'number'
    },
    {
        name: 'reelCenterY',
        title: 'Vertical Center of Reel Display Area',
        type: 'number'
    },
    {
        name: 'reelCentersX',
        title: 'Horizontal Center of each Reel Display Area',
        type: 'array'
    },
    {
        name: 'symbolSpacing',
        title: 'Vertical spacing between symbols in the reel',
        type: 'number'
    },
    {
        name: 'backgroundMusic',
        title: 'Background Music',
        type: 'sound',
    },
    {
        name: 'isMuted',
        title: 'Mute Audio by Default',
        type: 'boolean',
    },
    {
        name: 'controlColor',
        title: 'Control Color',
        type: 'color',
    },
    {
        name: 'reels',
        title: 'Reel Images',
        type: 'array',
        values: [
            { name: 'src', title: 'Image', type: 'image' },
            { name: 'name', title: 'Symbol Name', type: 'text' },
            { name: 'weight', title: 'Weight (chance of appearance)', type: 'number' }
        ]
    },
    {
        name: 'prizes',
        title: 'Payout List',
        type: 'array',
        values: [
            { name: 'names', title: 'Result Combination (space separated)', type: 'text' },
            { name: 'title', title: 'Payout', type: 'number' }
        ]
    },
    {
        name: 'payoutTableType',
        title: 'Payout Table Type',
        type: 'enum',
        enumValues: [{value: 'generated', title: 'Generated'}, {value: 'image', title: 'Image'}]
    },
    {
        name: 'payoutTableImage',
        title: 'Payout Table image',
        type: 'image'
    },
    {
        name: 'spinTime',
        title: 'Spin Time (before stopping)',
        type: 'number'
    },
    {
        name: 'speedMultiplier',
        title: 'Spin speed multiplier',
        type: 'number'
    },
    {
        name: 'strobeWhenSpinning',
        title: 'Use strobe effect when spinning',
        type: 'boolean'
    },
];

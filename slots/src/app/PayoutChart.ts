import { PayoutData, SymbolData } from './Config';

const STYLE = `
.payout-chart-overlay {
    background-color: rgba(0, 0, 0, 0.6);
    height: 100%;
    width: 100%;
    position: absolute;
    z-index: 1;
    display: none;
}

.payout-chart-overlay.show {
    display: block;
}

.payout-chart {
    margin-left: 50%;
    transform: translateX(-50%);
    position: absolute;
    top: 10%;
    /*bottom: 10%;*/
    max-height: 80%;
    width: 90%;
    max-width: 500px;
    overflow: auto;

    scrollbar-width: none; /* Firefox */
    -ms-overflow-style: none;  /* Internet Explorer 10+ */
}
.payout-chart.generated {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-auto-rows: auto;
    user-select: none;
    border: 2px gray solid;
    border-radius: 20px;
    background-color: white;
}
.payout-chart::-webkit-scrollbar { /* WebKit */
    width: 0;
    height: 0;
}

.payout-chart .image-chart {
    width: 100%;
    user-select: none;
    display: block;
}

.payout-chart-overlay .close {
    position: absolute;
    top: 10%;
    right: 5%;
    background-color: red;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    cursor: pointer;
}

.payout-chart-overlay .close .x-1 {
    height: 25px;
    width: 3px;
    margin-left: 13px;
    margin-top: 3px;
    background-color: white;
    transform: rotate(45deg);
    Z-index: 1;
}

.payout-chart-overlay .close .x-2 {
    height: 25px;
    width: 3px;
    background-color: white;
    transform: rotate(90deg);
    Z-index: 2;
}

.payout-column {
    padding: 3px;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-around;
    align-items: center;

    border-right: 2px gray solid;
    border-bottom: 2px gray solid;
}

.payout-column img {
    /* default size (overridden for 1-4 children below) */
    height: 12px;
}

/* one item */
.payout-column img:first-child:nth-last-child(1) {
	height: 30px
}

/* two items */
.payout-column img:first-child:nth-last-child(2),
.payout-column img:first-child:nth-last-child(2) ~ img {
	height: 30px
}

/* three items */
.payout-column img:first-child:nth-last-child(3),
.payout-column img:first-child:nth-last-child(3) ~ img {
	height: 20px
}

/* four items */
.payout-column img:first-child:nth-last-child(4),
.payout-column img:first-child:nth-last-child(4) ~ img {
	height: 20px;
}

.payout-result {
    display: flex;
    justify-content: space-around;
    align-items: center;
    padding: 3px;
    font-family: sans-serif;
    font-weight: 600;
    border-bottom: 2px gray solid;
}

.payout-result img {
    height: 30px;
}
`;

export class PayoutChart
{
    private overlay: HTMLDivElement;
    private root: HTMLDivElement;
    private image: HTMLImageElement;
    private close: HTMLDivElement;
    private data: PayoutData[];
    private symbolImages: {[name: string]: string};
    private visible: boolean;

    constructor(parent: HTMLDivElement)
    {
        // apply style
        const style = document.createElement('style');
        style.textContent = STYLE;
        document.head.append(style);

        this.overlay = document.createElement('div');
        this.overlay.classList.add('payout-chart-overlay');
        parent.appendChild(this.overlay);

        this.root = document.createElement('div');
        this.root.classList.add('payout-chart');
        this.overlay.appendChild(this.root);
        this.visible = false;
        this.data = [];
        this.symbolImages = {};

        this.image = new Image();
        this.image.className = 'image-chart';

        this.close = document.createElement('div');
        this.close.className = 'close';
        this.close.innerHTML = '<div class="x-1"><div class="x-2"></div></div>';
        // this.overlay.appendChild(this.close);

        this.close.addEventListener('click', () => this.hide());
        this.overlay.addEventListener('click', () => this.hide());
    }

    public show()
    {
        this.visible = true;
        this.overlay.classList.add('show');
    }

    public hide()
    {
        this.visible = false;
        this.overlay.classList.remove('show');
    }

    public setImage(src: string)
    {
        this.emptyRoot();
        this.root.classList.remove('generated');
        this.root.appendChild(this.image);
        if (this.image.src === src) return Promise.resolve();
        return new Promise(resolve => {
            if (src)
            {
                this.image.onload = resolve;
                this.image.src = src;
            }
            else
            {
                this.image.src = '';
                resolve();
            }
        });
    }

    public setData(data: PayoutData[])
    {
        if (!data)
        {
            data = [];
        }
        this.data = data;
        this.emptyRoot();
        this.root.classList.add('generated');
        this.buildChart();
    }

    public set symbols(symbols: SymbolData[])
    {
        this.symbolImages = {};
        for (const s of symbols)
        {
            this.symbolImages[s.name] = s.src;
        }
        this.emptyRoot();
        this.buildChart();
    }

    private buildChart()
    {
        for (const payout of this.data)
        {
            for (const line of payout.names)
            {
                const columns = line.split(' ');
                for (const column of columns)
                {
                    const cell = document.createElement('div');
                    cell.classList.add('payout-column');
                    this.root.appendChild(cell);

                    const names = column.split(',');
                    for (const name of names)
                    {
                        const img = new Image();
                        img.src = this.symbolImages[name];
                        cell.appendChild(img);
                    }
                }
                const outCell = document.createElement('div');
                outCell.classList.add('payout-result');
                this.root.appendChild(outCell);
                if (payout.payoutImage)
                {
                    const img = new Image();
                    img.src = payout.payoutImage;
                    outCell.appendChild(img);
                }
                else
                {
                    outCell.innerText = payout.title;
                }
            }
        }
    }

    private emptyRoot()
    {
        while (this.root.firstChild)
        {
            this.root.removeChild(this.root.lastChild!);
        }
    }
}
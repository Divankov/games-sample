export class ExtPromise<T = void>
{
    public resolve: (result: T) => void;
    public reject: (error: any) => void;
    public promise: Promise<T>;

    constructor()
    {
        this.promise = new Promise((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
    }
}

export function wait(milliseconds: number)
{
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const EsmWebpackPlugin = require("@purtuga/esm-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (env, argv) => {
    const isDev = argv.mode === 'development';

    const copy = [
        // always include assets for this game
        { from: 'src/assets', to: 'assets' },
    ];
    // if developing, include framework for testing
    if (isDev) {
        copy.push({ from: 'src/testconfig.json' });
        copy.push({ from: 'node_modules/@imblox/test-widget/parent-container' });
    }

    return {

        entry: './src/app/index.ts',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'index.js',
            // this combination of library & libraryTarget with esm-webpack-plugin lets us import the library output using ES6 imports
            // in the browser
            library: 'LIB',
            libraryTarget: 'var',
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        mode: argv.mode,
        devtool: isDev ? 'inline-source-map' : '',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new CopyPlugin({
                patterns: copy
            }),
            new EsmWebpackPlugin({
                exclude(fileName) {
                    // Exclude if:
                    //  a. not a js file
                    //  b. is a devServer.hot file
                    return !/\.[cm]?js$/i.test(fileName) ||
                        /\.hot-update\.js$/i.test(fileName);
                },
                skipModule(fileName, module) {
                    return /[\\\/]webpack(-dev-server)?[\\\/]/.test(fileName);
                }
            }),
            // new BundleAnalyzerPlugin()
        ],
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 3000,
            hot: true,
            host: '0.0.0.0'
        },

        optimization: {
            minimize: !isDev
        }
    };
};
